<?php
	define('ROOT', dirname(__DIR__));
	require ROOT . '/app/App.php';
	require ROOT. '/config/captcha.php';
	App::load();

	$page=App::getRouter($_SERVER['REQUEST_URI']);

	$p = explode('.', $page);

	if ($p[0] == 'admin'){
		$controller = '\App\Controller\Admin\\' . ucfirst($p[1]) . 'Controller';
		$action = $p[2];
	}else{
		$controller = '\App\Controller\\' . ucfirst($p[0]) . 'Controller';
		$action = $p[1];
	}

	$controller = new $controller();
	$controller->$action();