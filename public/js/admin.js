$(document).ready(function(){
	//FORMULAIRE BOOTSTRAP ---------------------------------------------------------
	if(document.querySelector(".form-group") != null){
		$('.form-group').children('input, select').addClass('form-control');
		$('.form-control').each(function(index) {
			nom=$(this).attr('name');
			$(this).attr('id', nom);
			$(this).prev('label').attr('for', nom);
		});
	}

	//SUMMERNOTE -------------------------------------------------------------------
	if (document.querySelector('.summernote') !== null) {
		$('head').append('<link href="/include/summernote/summernote-bs4.css" rel="stylesheet">');
		nom=$(this).attr('name');
		$.getScript('/include/summernote/summernote-bs4.min.js', function(){
			$.getScript('/include/summernote/lang/summernote-fr-FR.js', function(){
				$('.summernote-description').summernote({
					lang: 'fr-FR',
					height: 250,
					name: 'test',
					toolbar: [
						['style', ['bold', 'italic', 'underline']],
						['font', ['superscript', 'subscript']],
						['fontsize', ['fontsize']],
						['color', ['color']],
						['para', ['ul', 'ol', 'paragraph']],
						['misc', ['undo','redo','codeview']]
					]
				});
				$('.summernote-matos').summernote({
					lang: 'fr-FR',
					height: 200,
					name: 'test',
					toolbar: [
						['style', ['bold', 'italic', 'underline']],
						['fontsize', ['fontsize']],
						['para', ['ul', 'ol', 'paragraph']],
						['misc', ['undo','redo','codeview']]
					]
				});
				$('.summernote-blog').summernote({
					lang: 'fr-FR',
					height: 500,
					name: 'test',
					toolbar: [
						['para', ['style','ul', 'ol', 'paragraph']],
						['style', ['bold', 'italic', 'underline']],
						['font', ['superscript', 'subscript']],
						['fontsize', ['fontsize']],
						['color', ['color']],
						['misc', ['undo','redo','codeview']]
					]
				});
			});
		});
	}

	//CONFIRMATION BUTTON DANGER -------------------------------------------------
	$('.btn-danger').click(function(event) {
		href=$(this).data('href');
		if(confirm('Êtes-vous sûre de vouloir supprimer cet élément ?')){
			window.location = href;
		}
	});
});