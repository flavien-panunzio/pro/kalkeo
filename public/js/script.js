$(document).ready(function(){
	//AJOUT JC ET CSS EN FONTION DU BESOIN -----------------------------------------
		if (document.querySelector('#map') !== null) {
			$('head').append('<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>');
			$('head').append('<link rel="stylesheet" href="//unpkg.com/leaflet-gesture-handling/dist/leaflet-gesture-handling.min.css" type="text/css">');
			$.getScript('/js/vendor.js', function(){
				$.getScript('/js/maps.js', function(){
					$('.load').slideUp();
				});
			});
		}else{
			$('.load').slideUp();
		}
		if (document.querySelector('#fullpage') !== null) {
			$('head').append('<link rel="stylesheet" href="/include/fullPage/fullpage.min.css">');
			$.getScript('/include/fullPage/fullpage.min.js', function(){
				new fullpage('#fullpage', {
					anchors: ['landing', 'kalkeo', 'speleologie', 'canyoning','contact'],
					slidesNavigation: true,
					navigation: true,
					navigationPosition: 'right',
					licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE'
				});
				$('.load').slideUp();
			});
		}else{
			$('.load').slideUp();
		}
	//target _blank ----------------------------------------------------------------
		$("a[href^='http://'], a[href^='https://']").attr("target","_blank");
	
	//CLASS ACTIVE NAV -------------------------------------------------------------
		var classActive = '/'+location.pathname.split('/')[1];
		$('#sidebar a[href="'+classActive+'"], .navbar a[href="'+classActive+'"]').parent('li').addClass('active');

	//NAV --------------------------------------------------------------------------
		$('#sidebarCollapse').on('click', function () {
			$('#sidebar, #content, #fp-nav, .burger').toggleClass('active');
		});
		$(window).on('hashchange',function(){ 
			$('#sidebar, #content, #fp-nav, .burger').removeClass('active');
		});
		$('.fp-tableCell').on('click',function(){ 
			$('#sidebar, #content, #fp-nav, .burger').removeClass('active');
		});


	//ICONE BURGER NAVBAR ----------------------------------------------------------
		$('#navbarSupportedContent').on('show.bs.collapse', function () {$('.burger').addClass('active');});
		$('#navbarSupportedContent').on('hide.bs.collapse', function () {$('.burger').removeClass('active');});

	//FILTRE -----------------------------------------------------------------------
		$(".filtres button").click(function() {
			filtre=$(this).attr('id');
			if (filtre =='0') {
				$(".filtres button").removeClass('active');
				$(this).addClass('active');
				$(".item, .leaflet-marker-icon").removeClass('d-none');
			}
			else{
				$(".filtres button").removeClass('active');
				$(this).addClass('active');
				$(".item, .leaflet-marker-icon").addClass("d-none");
				$(".niveau"+filtre).removeClass("d-none");
				$('.leaflet-marker-icon[title='+filtre+']').removeClass("d-none");
			}
		});
		$('.filtres button').each(function (index ){
			if($('.niveau'+index).length == 0 && index!=0){
				console.log(index)
				$("#"+index).addClass('d-none');
			}
		});
	
	//EASTER EGGS
		$.getScript('/include/konami.js', function () {
			var easter_egg = new Konami(function() { 
				$('body').css({
					"-webkit-transform": "rotateY(180deg)",
					"-ms-transform": "rotateY(180deg)",
					"-moz-transform": "rotateY(180deg)",
					"transform": "rotateY(180deg)",
					
					"-webkit-transition" :"1s ease-in-out",
					"-moz-transition" :"1s ease-in-out",
					"-o-transition" :"1s ease-in-out",
					"transition" :"1s ease-in-out"
				})
			});
		});
});