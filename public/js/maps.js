let $map = document.querySelector('#map')
class LeafletMap {
	constructor (){
		this.map = null
		this.bounds = []
	}
	async load (element){
		return new Promise((resolve, reject) => {
			$script('https://unpkg.com/leaflet@1.7.1/dist/leaflet.js', () => {
				$script('//unpkg.com/leaflet-gesture-handling', () => {
					this.map = L.map(element,{
						gestureHandling: true
					})
					L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
						attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
						accessToken: 'pk.eyJ1IjoiZnVyc29uaXRlIiwiYSI6ImNqcGJnZnRscjBlbW4za3BocTl3OXo0NmIifQ.XRMsCJ51pNuqZTBGZJBEnA',
						id: 'mapbox/outdoors-v11'
					}).addTo(this.map)
					resolve()
				})
			})
		})
	}
	addMarker (lat, lng, marker, lvl){
		let point = [lat,lng]
		this.bounds.push(point)
		return new LeafletMarker(point, this.map, marker, lvl)
	}
	center() {
		var pad=parseInt(document.querySelector('#map').dataset.padding)
		this.map.fitBounds(this.bounds,{padding:[pad,pad]})
		//this.map.scrollWheelZoom.disable();
		if(window.location.pathname.split("/").length == 3)
			this.map.setZoom(12);
	}
}
class LeafletMarker {
	constructor(point, map, marker, lvl){
		var myIcon = L.icon({
			iconUrl: '/images/markers/'+marker+'.svg',
			iconSize: [25, 41],
			iconAnchor: [12, 41],
		});
		this.marker = L.marker(point, {
			icon: myIcon,
			title: lvl
		}).addTo(map)
	}
	setActive(){
		this.marker.getElement().classList.add('is-active')
	}
	unsetActive(){
		this.marker.getElement().classList.remove('is-active')
	}
	addEventListener(event, cb) {
		this.marker.addEventListener('add',() =>{
			this.marker.getElement().addEventListener(event, cb)	
		})
	} 
}
const initMap = async function(){
	let map = new LeafletMap()
	let hoverMarker = null
	await map.load($map)
	Array.from(document.querySelectorAll('.js-marker')).forEach((item) => {
		let marker = map.addMarker(item.dataset.lat, item.dataset.lng, item.dataset.marker, item.dataset.lvl)
		item.addEventListener('mouseover', function(){
			if (hoverMarker!==null) {
				hoverMarker.unsetActive()
			}
			marker.setActive()
			hoverMarker = marker
		})
		item.addEventListener('mouseleave', function(){
			if (hoverMarker!==null) {
				hoverMarker.unsetActive()
			}
		})
		marker.addEventListener('click', function(){
			if (item.dataset.link) {
				document.location.href = item.dataset.link;
			}
			if (item.dataset.marker == "speleologie-car" || item.dataset.marker == "canyoning-car"){
				os=getOS();
				if(document.location.href.split('/').length == 5){
					coords = item.dataset.lat + ',' + item.dataset.lng
					nom = $("h1").text()
					if (os=='Android') {
						document.location.href = 'geo:0,0?q=' + coords + '(' + nom + ')'
					}else if(os=='iOS'){
						document.location.href = 'maps://?ll=' + coords + '&q=' + nom
					}else{
						window.open('https://www.google.com/maps/dir/?api=1&destination='+coords);
					}
				}else{
					document.location.href = window.location.protocol+'//'+window.location.hostname+'/'+item.dataset.link
				}
			}
		})
	})
	map.center()
}
if ($map !== null) {
	initMap();
}

function getOS() {
	var userAgent = window.navigator.userAgent,
			platform = window.navigator.platform,
			macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
			windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
			iosPlatforms = ['iPhone', 'iPad', 'iPod'],
			os = null;
	if (macosPlatforms.indexOf(platform) !== -1) {
		os = 'MacOS';
	}else if (iosPlatforms.indexOf(platform) !== -1) {
		os = 'iOS';
	}else if (windowsPlatforms.indexOf(platform) !== -1) {
		os = 'Windows';
	}else if (/Android/.test(userAgent)) {
		os = 'Android';
	}else if (!os && /Linux/.test(platform)) {
		os = 'Linux';
	}
	return os;
}