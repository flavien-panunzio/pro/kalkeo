$(document).ready(function(){
	$(".upload-images img").on('click', function() {
		srcOrigin=$(this);
		$('#Modal-files').modal('show');
		img=$(this).attr('src');
		$('#Modal-files .modal-header img').attr('src', img);
		nomImage=$(this).data('nom');
		folder=$(this).data('folder');
		$("#dowload").attr('download', img.split("/")[img.split("/").length-1]).attr('href', window.location.hostname+img);;
		var files;
		var formData;
		$('input[type=file]').change(function(event) {
			files = event.target.files;
			$(this).prev('label').text(files[0].name);
			formData = new FormData($("#upload-image-modal")[0]);
			formData.set('fileToUpload', files[0], nomImage);
			formData.append("folder", folder);
			$('#Modal-files .modal-header img').attr('src', URL.createObjectURL(event.target.files[0]));
		});
		$('#Modal-files form').submit(function(event) {
			event.preventDefault();
		 	$.ajax({
				type: 'post',
				url: '/admin/upload',
				data: formData,
				contentType: false,
				processData: false,
				success: function (data) {
					$(srcOrigin).attr('src', "/"+folder+nomImage+"?"+new Date().getTime());
					$('#Modal-files').modal('hide');
					return false;
				}
			});
		});
	});
});