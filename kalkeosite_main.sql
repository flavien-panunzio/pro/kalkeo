-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  Dim 24 fév. 2019 à 20:38
-- Version du serveur :  10.2.22-MariaDB
-- Version de PHP :  7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `kalkeosite_main`
--

-- --------------------------------------------------------

--
-- Structure de la table `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `titre` text DEFAULT NULL,
  `contenu` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `slug` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `blog`
--

INSERT INTO `blog` (`id`, `titre`, `contenu`, `image`, `slug`) VALUES
(1, 'TEST1.1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget rutrum purus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam quis nibh vehicula, elementum ligula sit amet, sollicitudin dui. In gravida libero nulla, at egestas mauris blandit ut. Curabitur dapibus dignissim odio, sit amet vestibulum lectus pretium sit amet. Ut velit erat, vestibulum a faucibus sed, fermentum eu turpis. Vestibulum velit neque, tincidunt non urna quis, ultrices hendrerit mi. Nam eu leo scelerisque diam varius iaculis vel quis ligula. Curabitur vel orci vitae eros egestas consectetur ac at arcu.\r\n\r\nSed vulputate tellus sit amet neque consequat aliquet. Donec semper, nibh ac gravida varius, velit lectus gravida orci, vitae pellentesque enim enim at lacus. In vitae risus dictum, ultrices turpis id, consectetur purus. Nullam metus tortor, egestas non neque quis, pellentesque congue sem. Duis venenatis quam at placerat vestibulum. Mauris ornare libero ut tempor tempus. Vivamus tristique rutrum felis, non malesuada lacus laoreet vel. Suspendisse feugiat in turpis ut sagittis. Nulla vehicula ligula et facilisis sagittis. Nam ultrices, nulla eget fermentum tincidunt, ipsum tellus finibus orci, in rhoncus risus orci sit amet urna. Nam a dui at tortor fermentum dictum ac in nibh. Aliquam in nunc semper, ultricies ipsum sit amet, sagittis tortor. Suspendisse at pharetra felis.', 'https://unsplash.it/500/500?random=', 'test1'),
(2, 'TEST2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget rutrum purus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam quis nibh vehicula, elementum ligula sit amet, sollicitudin dui. In gravida libero nulla, at egestas mauris blandit ut. Curabitur dapibus dignissim odio, sit amet vestibulum lectus pretium sit amet. Ut velit erat, vestibulum a faucibus sed, fermentum eu turpis. Vestibulum velit neque, tincidunt non urna quis, ultrices hendrerit mi. Nam eu leo scelerisque diam varius iaculis vel quis ligula. Curabitur vel orci vitae eros egestas consectetur ac at arcu.\r\n\r\nSed vulputate tellus sit amet neque consequat aliquet. Donec semper, nibh ac gravida varius, velit lectus gravida orci, vitae pellentesque enim enim at lacus. In vitae risus dictum, ultrices turpis id, consectetur purus. Nullam metus tortor, egestas non neque quis, pellentesque congue sem. Duis venenatis quam at placerat vestibulum. Mauris ornare libero ut tempor tempus. Vivamus tristique rutrum felis, non malesuada lacus laoreet vel. Suspendisse feugiat in turpis ut sagittis. Nulla vehicula ligula et facilisis sagittis. Nam ultrices, nulla eget fermentum tincidunt, ipsum tellus finibus orci, in rhoncus risus orci sit amet urna. Nam a dui at tortor fermentum dictum ac in nibh. Aliquam in nunc semper, ultricies ipsum sit amet, sagittis tortor. Suspendisse at pharetra felis.', 'https://unsplash.it/500/500?random=', 'test2');

-- --------------------------------------------------------

--
-- Structure de la table `canyon`
--

DROP TABLE IF EXISTS `canyon`;
CREATE TABLE `canyon` (
  `id` int(11) NOT NULL,
  `type` varchar(6) NOT NULL DEFAULT 'canyon',
  `nom` varchar(255) DEFAULT NULL,
  `latitude` double DEFAULT 48.8627,
  `longitude` double DEFAULT 2.28759,
  `description` text DEFAULT NULL,
  `niveau` int(11) DEFAULT NULL,
  `slug` text DEFAULT NULL,
  `duree` varchar(20) DEFAULT 'Journée',
  `age` int(11) DEFAULT 18,
  `tarif_solo` int(11) DEFAULT 150,
  `tarif_groupe` int(11) DEFAULT 100,
  `nb_groupe_min` int(11) DEFAULT 4,
  `temps_aller` int(11) DEFAULT NULL,
  `temps_retour` int(11) DEFAULT NULL,
  `denivele` int(11) DEFAULT NULL,
  `obstacles` text DEFAULT NULL,
  `commune` text DEFAULT NULL,
  `departement` text DEFAULT NULL,
  `aprevoir` text DEFAULT NULL,
  `compris` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `canyon`
--

INSERT INTO `canyon` (`id`, `type`, `nom`, `latitude`, `longitude`, `description`, `niveau`, `slug`, `duree`, `age`, `tarif_solo`, `tarif_groupe`, `nb_groupe_min`, `temps_aller`, `temps_retour`, `denivele`, `obstacles`, `commune`, `departement`, `aprevoir`, `compris`) VALUES
(1, 'canyon', 'Canyon de Terneze', 45.560001373291016, 6.024320125579834, NULL, 1, 'canyon_de_terneze', 'Demi-journée', 6, 45, 40, 4, 20, 30, 30, 'sauts, nage, toboggans', 'Curienne', 'Savoie', NULL, NULL),
(2, 'canyon', 'Canyon du Furon (haut)', 45.19929885864258, 5.631609916687012, NULL, 2, 'canyon_du_furon_haut', 'Demi-journée', 12, 48, 43, 6, 25, 10, 100, 'Descente en rappel, sauts, nage, toboggans', 'Engins', 'Isère', NULL, NULL),
(3, 'canyon', 'Canyon du Versoud', 45.22060012817383, 5.499780178070068, '<p class=\"MsoNormal\">Le canyon du Versoud sur la commune de « La Rivière », est l’un des canyons les plus prisés du Vercors.</p><p class=\"MsoNormal\">Niché au creux d’une forêt verdoyante et sur plombé par un magnifique pont en pierre sèches qui marque la fin de l’encaissement, ce joli canyon est un incontournable de la région pour la pratique du canyoning.<o:p></o:p></p><p></p><p class=\"MsoNormal\">Ce parcours assez court, convient à la découverte de l’activité pour les grands comme pour les petits mais aussi à un public un peu plus sportif et désireux de vivre une demi-journée sensationnelle. A 25 min de Grenoble et à 45min de Valence, le canyon du Versoud est une destination idéale pour partager de bons moments en famille ou entre amis. Les moniteurs de KalkeO vous encadrent en toute sécurité mais ils vont également vous partager leurs passions et vous sensibiliser un environnement naturel sensible.</p>', 1, 'canyon_du_versoud', 'Demi-journée', 8, 48, 43, 6, 15, 5, 80, 'Descente en rappel, sauts, nage, toboggans', 'La Rivière', 'Isère', '<ul><li>un maillot de bain</li><li>des baskets</li><li>une serviette</li><li>des affaires de rechange</li><li>de l\'eau</li></ul>', '<ul><li>l\'encadrement par un moniteur diplômé </li><li>un équipement adapté de qualité</li><li>une assurance individuelle</li><li>les photos de la sortie</li><li>le sourire du moniteur :)</li></ul>'),
(4, 'canyon', 'Canyon des Ecouges 2', 45.18040084838867, 5.489389896392822, NULL, 3, 'canyon_des_ecouges_2', 'Demi-journée', 14, 70, 65, 4, 15, 15, 350, 'Sauts, rappels, toboggans', 'Saint Gervais ', 'Isère', NULL, NULL),
(5, 'canyon', 'Canyon de l’Infernet', 45.25310134887695, 5.715489864349365, '<p>Un jolie canyon niché au fond d\'une gorge profonde ! </p><p>Le canyon de l\'Infernet est typique par </p><p><br></p>', 3, 'canyon_de_l_infernet', 'Demi-journée', 16, 55, 50, 4, 15, 30, 70, 'descente en rappel, sauts, nage, toboggans', 'quaix en chartreuse', 'Isère', NULL, NULL),
(6, 'canyon', 'Canyon de la Pissarde', 45.10850143432617, 5.654850006103516, NULL, 3, 'canyon_de_la_pissarde', NULL, 17, 70, 65, 4, 20, 15, 320, 'Descente en rappel d\'envergure', 'Claix (38640)', 'Isère', '<ul><li><i>un maillot de bain</i></li><li><i>des baskets (type running ou randonnée)</i></li><li><i>une serviette</i></li><li><i>des affaires de rechange</i></li><li><i>de l\'eau</i></li></ul>', '<ul><li><i>l\'encadrement par un moniteur diplômé </i></li><li><i>un équipement adapté de qualité</i></li><li><i>une assurance individuelle</i></li><li><i>les photos de la sortie</i></li><li><i>le sourire du moniteur :)</i></li></ul>'),
(7, 'canyon', 'Canyon du Pont du Diable', 45.72669982910156, 6.100639820098877, '<p>Et oui le canyoning, c\'est en Savoie aussi !</p><p>En plein cœur de la Savoie,à 45 min de Chambéry et 30 min d\'Annecy, le canyon du Pont du Diable offre un parcours d\'initiation avec quelques beaux obstacles. Nous découvrons ce canyon, une fois sur le pont qui surplombe la gorge quelque peu inquiétante (d\'où son nom) mais c\'est une fois au fond de celle-ci que nous découvrons la beauté de ces lieux. L\'eau est venue creuser son passage à travers la roche et a laissé derrière elle un beau terrain de jeu pour les amateurs de canyoning. Les sauts,les descente en rappel,les toboggans et la nage rythmerons la descente dans une ambiance féerique.</p><p>Rendez vous sur le parking du canyon (voir carte).</p>', 2, 'canyon_du_pont_du_diable', 'Demi-journée', 14, 50, 45, 6, 10, 15, NULL, 'Descente en rappel, sauts, nage, toboggans', 'Bellecombe en bauges (73340)', 'Savoie', '<ul><li><i>un maillot de bain</i></li><li><i>des baskets (type running ou randonnée)</i></li><li><i>une serviette</i></li><li><i>des affaires de rechange</i></li><li><i>de l\'eau</i></li></ul>', '<ul><li><i>l\'encadrement par un moniteur diplômé </i></li><li><i>un équipement adapté de qualité</i></li><li><i>une assurance individuelle</i></li><li><i>les photos de la sortie</i></li><li><i>le sourire du moniteur :)</i></li></ul>'),
(8, 'canyon', 'Canyon des Moules Marinière', 44.9547004699707, 5.569509983062744, '<p>Une belle demi-journée au frais !</p><p>Au cœur du massif du Vercors, le canyon des Moules Marinières est un canyon d\'initiation idéale pour découvrir l\'activité. Un petit torrent qui file à travers une forêt magnifique, une ambiance calme et un parcours adapté à tous, font de ce lieu un moment hors du temps. Sur la commune de Saint Andéol (38650), ce parcours se termine par deux beaux obstacles qui, selon les années, se passe en sautant en glissant ou en utilisant la corde. Chaque année la nature,&nbsp;<span style=\"font-size: 1rem;\">nous réserve de belles surprises.</span></p><p><span style=\"font-size: 1rem;\">Rendev vous au départ du canyon (voir carte).</span></p>', 1, 'canyon_des_moules_mariniere', 'Demi-journée', 12, 48, 43, 6, 10, 5, 180, 'Sauts, rappels, toboggans', 'Saint andéol', 'Isère', '<ul><li><i>un maillot de bain</i></li><li><i>des baskets (type running ou randonnée)</i></li><li><i>une serviette</i></li><li><i>des affaires de rechange</i></li><li><i>de l\'eau</i></li></ul>', '<ul><li><i>l\'encadrement par un moniteur diplômé </i></li><li><i>un équipement adapté de qualité</i></li><li><i>une assurance individuelle</i></li><li><i>les photos de la sortie</i></li><li><i>le sourire du moniteur :)</i></li></ul>'),
(9, 'canyon', 'Canyon du Furon (bas)', 45.19929885864258, 5.631609916687012, '<p>Un canyon dans la ville !</p><p>Au départ du parking de l\'église de Sassenage (38360), le canyon du Furon offre l’opportunité de découvrir l\'activité sans forcément prendre la voiture pour les Grenoblois. C\'est une chance d\'avoir un petit coin de paradis au portes de la ville. Ce canyon permet de découvrir les joies du canyoning et de vous initier tout en douceur à la descente en rappel. Idéal si vous avez déjà pratiqué le Furon Haut. N\'hésitez pas à me contacter pour vous aider à choisir.</p><p><br></p>', 2, 'canyon_du_furon_bas', 'Demi-journée', 14, 48, 43, 6, 25, 5, 180, 'descente en rappel, sauts, nage', 'Sassenage', 'Isère', '<ul><li><i>un maillot de bain</i></li><li><i>des baskets (type running ou randonnée)</i></li><li><i>une serviette</i></li><li><i>des affaires de rechange</i></li><li><i>de l\'eau</i></li></ul>', '<ul><li><i>l\'encadrement par un moniteur diplômé </i></li><li><i>un équipement adapté de qualité</i></li><li><i>une assurance individuelle</i></li><li><i>les photos de la sortie</i></li><li><i>le sourire du moniteur :)</i></li></ul>'),
(10, 'canyon', 'Canyon du Furon (intégrale)', 45.2063, 5.65761, '<p>Un havre de fraîcheur aux portes de Grenoble !</p><p>L\'intégrale du canyon du Furon se déroule en deux parties. Nous commençons par la partie basse, au cœur d\'une foret verdoyante nous découvrons l\'activité tout en douceur et les différents obstacles demandent souvent l\'utilisation de la corde. Maintenant que vous êtes des experts en descente en rappel, nous allons arpenter la partie haute du canyon et jongler entre sauts, toboggans et petits passages secrets. Les deux parties sont très complémentaires et permettent de découvrir l\'activité dans son intégralité. </p><p>Le rendez vous est fixé sur le parking derrière l\'église de Sassenage.</p>', 3, 'canyon_du_furon_integral', 'Journée', 14, 75, 70, 6, 15, 5, 300, 'Descente en rappel, sauts, nage, toboggans', 'Sassenage, Engins', 'Isère', '<ul><li><i>un maillot de bain</i></li><li><i>des baskets (type running ou randonnée)</i></li><li><i>une serviette</i></li><li><i>des affaires de rechange</i></li><li><i>de l\'eau</i></li><li><i>un pique nique( type sandwichs), barres de céréales...</i></li></ul>', '<ul><li><i>l\'encadrement par un moniteur diplômé </i></li><li><i>un équipement adapté de qualité</i></li><li><i>une assurance individuelle</i></li><li><i>les photos de la sortie</i></li><li><i>le sourire du moniteur :)</i></li></ul>'),
(11, 'canyon', 'Canyon des Ecouges (intégrales)', 45.201744079589844, 5.481366157531738, '<p>Une journée varié </p><p>Vous avez du mal à vous décider entre les différents parcours du canyon des Ecouges? Vous êtes sportif et vous avez déjà pratiqué l\'activité, n\'hésitez plus et venez parcourir le canyon des Ecouges dans son intégralité. Grâce à ce parcours vous l\'avez vous essayer à tous les franchissements possibles : sauts, toboggans, descente en rappel d\'envergure, nage et marche de terrain varié. C\'est une vraie partie de plaisir et une belle journée perspective. Tentez l\'expérience avec KalkeO.</p><p>Toutes les informations seront rappelé la veille par mail.</p>', 4, 'canyon_des_ecouges_integrales', 'Journée', 16, 120, 110, 4, 5, 15, 600, 'Descente en rappel, sauts, nage, toboggans', 'Saint Gervais (38470)', 'Isère', '<ul><li><i>un maillot de bain</i></li><li><i>des baskets (type running ou randonnée)</i></li><li><i>une serviette</i></li><li><i>des affaires de rechange</i></li><li><i>de l\'eau</i></li><li><i>un pique nique( type sandwichs), barres de céréales...</i></li></ul>', '<div><ul><li><i>l\'encadrement par un moniteur diplômé </i></li><li><i>un équipement adapté de qualité</i></li><li><i>une assurance individuelle</i></li><li><i>les photos de la sortie</i></li><li><i>le sourire du moniteur :)</i></li></ul></div>'),
(12, 'Canyon', 'Canyon des Ecouges 1', 45.201, 5.489, '<p>Une grande course vertigineuse !</p><p>Les Ecouges 1 est la partie supérieur du canyon des Ecouges. Très encaissée et entrecoupée de grandes cascades, c\'est une approche très sportive de l\'activité qui est adaptée aux personnes recherchant des sensations fortes. Un itinéraire dans une gorge étroite parfois haute de plusieurs dizaines de mètres, un enchaînement d\'obstacles soutenu et une grande cascade finale de 65m, vous permettra de vivre une journée sportive dans un cadre naturel. Prévoyez la journée et soyez en forme !</p>', 4, 'canyon_des_ecouges_1', 'journée', 16, 95, 90, 4, 5, 5, 300, 'Descente en rappel d\'envergure', 'Saint Gervais (38470)', 'Isère', '<ul><li><i>un maillot de bain</i></li><li><i>des baskets (type running ou randonnée)</i></li><li><i>une serviette</i></li><li><i>des affaires de rechange</i></li><li><i>de l\'eau</i></li><li><i>un pique nique( type sandwichs), barres de céréales...</i></li></ul>', '<ul><li><i>l\'encadrement par un moniteur diplômé </i></li><li><i>un équipement adapté de qualité</i></li><li><i>une assurance individuelle</i></li><li><i>les photos de la sortie</i></li><li><i>le sourire du moniteur :)</i></li></ul>'),
(13, 'Canyon', 'Canyon des Ecouges Intermédiaires', 45.201663970947266, 5.480999946594238, '<p>Le plus beau canyon de la région !</p><p>Le canyon des Ecouges est découpé en plusieurs parties. La partie intermédiaire permet de profiter au maximum de l\'eau, nous évoluons dans des cascades, des toboggans et traversons de belles vasques d\'eau turquoise. Certains pourrons s\'initier aux sauts. C\'est un itinéraire magnifique qui reste néanmoins sportif dans un environnement naturel exceptionnel. N\'hésitez pas à mon contacter pour définir ensemble la partie la plus adaptées à votre demande. </p><p>Rendez vous devant l\'église de Saint Gervais (38470).</p>', 2, 'canyon_des_ecouges_intermediaires', 'Demi-journée', 12, 60, 55, 6, 10, 15, 200, 'Descente en rappel, sauts, nage, toboggans', 'Saint Gervais (38470)', 'Isère', '<ul><li><span style=\"font-size: 14px;\"><i>un maillot de bain</i></span></li><li><span style=\"font-size: 14px;\"><i>des baskets</i></span></li><li><span style=\"font-size: 14px;\"><i>une serviette</i></span></li><li><span style=\"font-size: 14px;\"><i>des affaires de rechange</i></span></li><li><span style=\"font-size: 14px;\"><i>de l\'eau</i></span></li></ul>', '<ul><li><span style=\"font-size: 14px;\"><i>l\'encadrement par un moniteur diplômé </i></span></li><li><span style=\"font-size: 14px;\"><i>un équipement adapté de qualité</i></span></li><li><span style=\"font-size: 14px;\"><i>une assurance individuelle</i></span></li><li><span style=\"font-size: 14px;\"><i>les photos de la sortie</i></span></li><li><span style=\"font-size: 14px;\"><i>le sourire du moniteur :)</i></span></li></ul>');

-- --------------------------------------------------------

--
-- Structure de la table `speleo`
--

DROP TABLE IF EXISTS `speleo`;
CREATE TABLE `speleo` (
  `id` int(11) NOT NULL,
  `type` varchar(6) NOT NULL DEFAULT 'speleo',
  `nom` varchar(255) DEFAULT NULL,
  `latitude` double DEFAULT 48.8627,
  `longitude` double DEFAULT 2.28759,
  `description` text DEFAULT NULL,
  `niveau` int(11) DEFAULT NULL,
  `slug` text DEFAULT NULL,
  `duree` varchar(20) DEFAULT 'Journée',
  `age` int(11) DEFAULT 18,
  `tarif_solo` int(11) DEFAULT 150,
  `tarif_groupe` int(11) DEFAULT 100,
  `nb_groupe_min` int(11) DEFAULT 4,
  `temps_aller` int(11) DEFAULT NULL,
  `temps_retour` int(11) DEFAULT NULL,
  `denivele` int(11) DEFAULT NULL,
  `obstacles` text DEFAULT NULL,
  `commune` text DEFAULT NULL,
  `departement` text DEFAULT NULL,
  `aprevoir` text DEFAULT NULL,
  `compris` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `speleo`
--

INSERT INTO `speleo` (`id`, `type`, `nom`, `latitude`, `longitude`, `description`, `niveau`, `slug`, `duree`, `age`, `tarif_solo`, `tarif_groupe`, `nb_groupe_min`, `temps_aller`, `temps_retour`, `denivele`, `obstacles`, `commune`, `departement`, `aprevoir`, `compris`) VALUES
(1, 'speleo', 'Grotte Roche', 45.07109832763672, 5.498760223388672, NULL, 1, 'grotte_roche', 'Demi-journée', 6, 48, 43, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'speleo', 'Les Cuves de Sassenage', 45.20819854736328, 5.6570000648498535, NULL, 1, 'les_cuves_de_sassenage', 'Demi-journée', 6, 45, 40, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'speleo', 'Grotte de Prérouge', 45.72480010986328, 6.094910144805908, NULL, 1, 'grotte_de_prerouge', 'Journée', 14, 75, 70, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'speleo', 'Grotte des Eymards', 45.129254, 5.584802, NULL, 2, 'grotte_des_eymards', 'Journée', 14, 75, 70, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'speleo', 'Grotte de Gournier', 45.0703010559082, 5.397960186004639, NULL, 3, 'grotte_de_gournier', 'Journée', 14, 80, 75, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'speleo', 'La traversée de la dent de Crolles', 45.304500579833984, 5.838560104370117, NULL, 3, 'traversee_de_la_dent_de_crolles', 'Journée', 18, 150, 100, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'speleo', 'Le Gour Fumant', 45.02470016479492, 5.471819877624512, NULL, 2, 'le_gour_fumant', 'Journée', 14, 105, 100, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'speleo', 'Les Saintes glaces', 45.15079879760742, 5.5181097984313965, NULL, 3, 'les_saintes_glaces', 'Journée', 16, 120, 110, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--


--
-- Index pour les tables déchargées
--

--
-- Index pour la table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `canyon`
--
ALTER TABLE `canyon`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `speleo`
--
ALTER TABLE `speleo`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `canyon`
--
ALTER TABLE `canyon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `speleo`
--
ALTER TABLE `speleo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
