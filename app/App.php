<?php
	use Core\Config;
	use Core\Database\MysqlDatabase;

	class App {
		private $db_instance;
		private  static $_instance;
		

		public static function getInstance(){
			if (is_null(self::$_instance)){
				self::$_instance = new App();
			}
			return self::$_instance;
		}

		public static function load(){
			session_start();
			require ROOT.'/app/Autoloader.php';
			App\Autoloader::register();
			require ROOT.'/core/Autoloader.php';
			Core\Autoloader::register();
			if(strrpos($_SERVER['REQUEST_URI'], '?'))
				$_SERVER['REQUEST_URI'] = explode('?', $_SERVER['REQUEST_URI'])[0];
		}

		public function getTable($name){
			$class_name='\\App\\Table\\'.ucfirst($name). 'Table';
			return new $class_name($this->getDb());
		}

		public function getDb(){
			$config=Config::getInstance(ROOT. '/config/config.php');
			if (is_null($this->db_instance))
				$this->db_instance = new MysqlDatabase($config->get('db_name'),$config->get('db_user'),$config->get('db_pass'),$config->get('db_host'));
			return $this->db_instance;
		}

		public static function getRouter($req){
			$router = require ROOT. '/config/router.php';
			$req = explode('/', $req);
			array_shift($req);
			$slug='/'.$req[0];
			if(array_key_exists($slug, $router) ){
				if($slug=='/admin'){
					
					if(!isset($req[1]))
						header('Location: /admin/index');
					elseif(array_key_exists($req[1], $router->$slug) ){
						if(isset($req[2]))
							$_GET['id']=$req[2];
						$admin = explode('/', $slug)[1];
						$method = $router->$slug[$req[1]]['method'];
						$controller = $router->$slug[$req[1]]['class'];
						return $admin.'.'.$controller.'.'.$method;
					}else
						header('Location: /admin/index');
				}
				if (isset($req[1])) {
					$id=explode('_', $req[1]);
					$_GET['id']=$id[0];
					$_GET['slug']=str_replace($id[0].'_','',$req[1]);
					return $router->$slug['class'].'.show';
				}else
					return $router->$slug['class'].'.'.$router->$slug['method'];
			}else{
				return 'Errors.error_404';
			}
		}
	}