<?php 
	namespace App\Controller;
	use Core\Controller\Controller;
	use Core\Table\Table;

	class BlogController extends AppController{
  
		public function __construct(){
			parent::__construct();
			$this->loadModel('Blog');
		}

		public function index(){
			$this->description="Besoin d’idées pour choisir votre sortie ? Venez prendre connaissance de nos articles sur cette page.";
			$this->titre='Blog | '.$this->titre;
			$blog=$this->Blog->all();
			$this->render('blog.index', compact('blog'));
		}

		public function show(){
			if($show=$this->Blog->find($_GET['id'])){
				if($show->slug!=$_GET['slug']){
					header("Status: 301 Moved Permanently", false, 301);
					header("Location: /blog/".$_GET['id'].'_'.$show->slug);
					die;
				}
				$this->description=strip_tags(substr($show->contenu,0,150)).'...';
				$this->titre=$show->titre.' | '.$this->titre;
				$this->render('blog.show', compact('show'));
			}else{
				header('location: /blog');
				die;
			}
		}
	}