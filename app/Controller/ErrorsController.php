<?php

namespace App\Controller;

use Core\Controller\Controller;

class ErrorsController extends AppController{

	public function __construct(){
		parent::__construct();
		$this->indexation='<meta name="robots" content="noindex">';
	}

	public function error_404(){
		$this->render('errors.404');
	}

	public function error_418(){
		$this->render('errors.418');
	}

	public function maintenance(){
		$this->render('errors.maintenance');
	}

}