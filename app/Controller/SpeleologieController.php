<?php 
	namespace App\Controller;
	use Core\Controller\Controller;
	use Core\Table\Table;

	class SpeleologieController extends AppController{
  
		public function __construct(){
			parent::__construct();
			$this->loadModel('Speleo');
		}

		public function index(){
			$this->description="KalkeO, c’est aussi la découverte du monde souterrain. Basé à Grenoble, au croisement de trois massifs calcaires, je vous emmène découvrir les secrets d’un monde parallèle.";
			$this->titre='Spéléologie | '.$this->titre;
			$index=$this->Speleo->all();
			$marker='speleologie';
			$this->render('lieux.index', compact('index','marker'));
		}

		public function show(){
			if($show=$this->Speleo->find($_GET['id'])){
				if($show->slug!=$_GET['slug']){
					header("Status: 301 Moved Permanently", false, 301);
					header("Location: /speleologie/".$_GET['id'].'_'.$show->slug);
					die;
				}
				$marker='speleologie';
				$this->description=strip_tags(explode('</p>', $show->description)[0]).'...';
				$this->titre=$show->nom.' | '.$this->titre;
				$this->render('lieux.show', compact('show','marker'));
			}else{
				header('location: /speleologie');
				die;
			}
		}
	}