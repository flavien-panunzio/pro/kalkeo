<?php 
	namespace App\Controller;

	use Core\Auth\DBAuth;
	use \App;


	class UsersController extends AppController{

		public function __construct(){
			parent::__construct();
			$this->indexation='<meta name="robots" content="noindex">';
			$this->template = 'users';
		}

		public function login(){
			$errors = false;
			if(isset($_SESSION['auth']) && !is_null($_SESSION['auth'])){
				header('Location: /admin/index');
				die;
			}
			if (!empty($_POST)) {
				$auth = new DBAuth(App::getInstance()->getDb());
				if($auth->login($_POST['username'], $_POST['password'])){
					header('Location: /admin/index');
					die;
				}else{
					$errors = true;
				}
			}
			$this->render('users.login',compact('errors'));
		}

		public function logout(){
			unset($_SESSION['auth']);
			header('Location: /');
			die;
		}
	}