<?php 
	namespace App\Controller\Admin;

	class PostsController extends AppController{

		public function __construct(){
			parent::__construct();
		}

		public function index(){
			$this->render('admin.posts.index');
		}

		public function equipe(){
			$this->titre="Equipe | ".$this->titre;
			$this->render('admin.posts.equipe');
		}

	}