<?php 
	namespace App\Controller\Admin;

	class LieuxController extends AppController{

		public function __construct(){
			parent::__construct();
			$this->loadModel('Canyon');
			$this->loadModel('Speleo');
			$this->loadModel('Blog');
			$this->titre="Lieux | ".$this->titre;
		}

		public function index(){
			$index = $this->Canyon->tout();
			$this->render('admin.lieux.index', compact('index'));
		}

		public function add(){
			$columns=$this->Canyon->nomColumn();
			$post=(object)array();
			foreach ($columns as $value) {
				if($value!="type" && $value!="visible")
					$post->{$value}=null;
			}
			$table = ucfirst($_GET['id']);
			$result = $this->{$table}->create($post);
			$id=$this->{$table}->lastid()->id;
			if ($result)
				header("Location: /admin/modiflieux/".$_GET["id"]."_".$id);
			else
				header('Location: /admin/lieux');
		}

		public function edit(){
			if(isset($_POST["ajax"])){
				foreach ($_POST["data"] as $value) {
					$temp=explode("|||", $value);
					$_POST[$temp[0]]=$temp[1];
				}
				unset($_POST["data"]);
				$id=$_POST["id"];
				$table= ucfirst($_POST["table"]);
			}else{
				$id=explode('_', $_GET['id'])[1];
				$table= ucfirst(explode('_', $_GET['id'])[0]);
			}

			if(($table=='Canyon' || $table=='Speleo') && $post=$this->{$table}->find($id)){
				if (!empty($_POST)) {
					foreach ($_POST as $key => $value) {
						if($key!='files' && $key!='ajax' && $key!="table" && $key!="id"){
							if($value=="")
								$value=null;
							$tableau[$key]=$value;
						}
					}
					$result = $this->{$table}->update($id, $tableau);
					if ($result && !isset($_POST["ajax"]))
						header('Location: /admin/lieux');
				}
				if(!isset($_POST["ajax"]))
					$this->render('admin.lieux.edit', compact('post', 'table'));
			}else
				header('Location: /admin/lieux');
		}

		public function delete(){
			$table= ucfirst(explode('_', $_GET['id'])[0]);
			$id=explode('_', $_GET['id'])[1];


			if(($table=='Canyon' || $table=='Speleo') && $post=$this->{$table}->find($id)){
				$dir = ROOT.'/public/';
				if (!is_dir($dir))
					$dir = ROOT.'/public_html/';
				if($table=="Canyon")
					$dir_lieu="images/lieux/canyoning/";
				if($table=="Speleo")
					$dir_lieu="images/lieux/speleologie/";
				for ($i=1; $i <= 4; $i++) { 
					$photo = $dir.$dir_lieu.$post->slug."_".$i.'.jpg';
					if (file_exists($photo)) {
						unlink($photo);
					}
				}
				$result = $this->{$table}->delete($id);
				header('Location: /admin/lieux');
			}
		}

		public function visible(){
			$id=explode('_',$_POST["id"])[0];
			$table=ucfirst(explode('_',$_POST["id"])[1]);
			$value=['visible' => explode('_',$_POST["id"])[2]];
			echo($id.$table);
			$this->{$table}->update($id, $value);
		}
	}