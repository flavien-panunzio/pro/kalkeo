<?php 
	namespace App\Controller\Admin;

	class BlogController extends AppController{

		public function __construct(){
			parent::__construct();
			$this->loadModel('Blog');
			$this->titre="Blog | ".$this->titre;
		}

		public function index(){
			$posts = $this->Blog->all();
			$this->render('admin.blog.index', compact('posts'));
		}

		public function add(){
			$post=(object)array();
			$post->titre=null;
			$post->contenu=null;
			$post->slug=null;
			$post->image=null;
			if (!empty($_POST)) {
				$result = $this->Blog->create([
					'titre' => $_POST['titre'],
					'contenu' => $_POST['contenu'],
					'slug' => $_POST['slug'],
					'image' => $_POST['image']
				]);
				if ($result) {
					header('Location: /admin/blog');
				}
			}
			$this->render('admin.blog.edit', compact('post'));
		}

		public function edit(){
			if (!empty($_POST)) {
				$result = $this->Blog->update($_GET['id'], [
					'titre' => $_POST['titre'],
					'contenu' => $_POST['contenu'],
					'slug' => $_POST['slug'],
					'image' => $_POST['image']
				]);
				if ($result) {
					return $this->index();
				}
			}
			$post = $this->Blog->find($_GET['id']);
			$this->render('admin.blog.edit', compact('post'));
		}

		public function delete(){
			if (!empty($_GET)) {
				$this->Blog->delete($_GET['id']);
			}
			header('Location: /admin/blog');
		}

	}