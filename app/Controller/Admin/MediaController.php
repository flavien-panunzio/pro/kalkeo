<?php 
	namespace App\Controller\Admin;

	class MediaController extends AppController{

		public function __construct(){
			parent::__construct();
		}

		public function index(){
			if (!isset($_GET['id']) || $_GET['id']=="") {
				$_GET['id']="/";
			}
			else
				$_GET['id']='/'.$_GET['id'].'/';
			$files=$this->readDIR('/'.$_GET['id']);
			$folder=$_GET['id'];
			$this->titre="Média | ".$this->titre;
			$this->render('admin.media.index', compact('files', 'folder'));
		}
		
		public function upload(){
			$target_dir = ROOT.'/public/'.$_POST["folder"];
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			// Check if image file is a actual image or fake image
			if(isset($_POST["submit"])) {
				$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
				if($check !== false) {
					echo "File is an image - " . $check["mime"] . ".";
					$uploadOk = 1;
				} else {
					echo "File is not an image.";
					$uploadOk = 0;
				}
			}
			$target_file = str_replace(' ', '_', $target_file);
			// Check if file already exists
			if (file_exists($target_file)) {
				unlink($target_file);
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 2097152) {
				echo "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "svg" && $imageFileType != "gif" ) {
				echo "Sorry, only JPG, JPEG, PNG, SVG & GIF files are allowed.";
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				echo "Sorry, your file was not uploaded.";
			// if everything is ok, try to upload file
			} else {
				if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
					echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
				} else {
					echo "Sorry, there was an error uploading your file.";
				}
			}
		}

		public function readDIR($DIRname=''){
			$i=0;
			if($dossier = opendir('images/'.$DIRname)){
				while(false !== ($fichier = readdir($dossier))) {
					if($fichier != '.' && $fichier != '/' && $fichier != '..' && $fichier != 'lieux' && $fichier != 'general'){
						$fichiers[$i]=$fichier;
						$i++;
					}
				}
				if (isset($fichiers)) {
					return $fichiers;
				}
			}
		}

		public function pdf(){
			$target_dir = ROOT.'/public/download/';
			$target_file = $target_dir . basename('diplome-manon_roche.pdf');
			$uploadOk = 1;
			$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			$target_file = str_replace(' ', '_', $target_file);
			if (file_exists($target_file))
				unlink($target_file);
			if($fileType != "pdf")
				echo "Sorry, only JPG, JPEG, PNG, SVG & GIF files are allowed.";
			else{
				if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)){
					header("Cache-Control: no-cache, must-revalidate");
					header('Location: /admin/equipe');
				}
				else
					echo "Sorry, there was an error uploading your file.";
			}
		}
	}