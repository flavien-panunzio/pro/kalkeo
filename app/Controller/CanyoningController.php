<?php 
	namespace App\Controller;
	use Core\Controller\Controller;
	use Core\Table\Table;

	class CanyoningController extends AppController{
  
		public function __construct(){
			parent::__construct();
			$this->loadModel('Canyon');
		}

		public function index(){
			$this->description="Voici tous nos sites de pratique pour le canyoning, dans le massif du Vercors, de la Chartreuse ou des Bauges, adaptés à votre demande. KalkeO vous emmène en tout sécurité.";
			$this->titre='Canyoning | '.$this->titre;
			$index=$this->Canyon->all();
			$marker='canyoning';
			$this->render('lieux.index', compact('index','marker'));
		}

		public function show(){
			if($show=$this->Canyon->find($_GET['id'])){
				if($show->slug!=$_GET['slug']){
					header("Status: 301 Moved Permanently", false, 301);
					header("Location: /canyoning/".$_GET['id'].'_'.$show->slug);
					die;
				}
				$marker='canyoning';
				$this->description=strip_tags(explode('</p>', $show->description)[0]).'...';
				$this->titre=$show->nom.' | '.$this->titre;
				$this->render('lieux.show', compact('show','marker'));
			}else{
				header('location: /canyoning');
				die;
			}
		}
	}