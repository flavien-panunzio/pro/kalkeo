<?php 
	namespace App\Controller;
	use Core\Controller\Controller;
	use Core\Table\Table;

	class PostsController extends AppController{

		public function __construct(){
			parent::__construct();
			$this->loadModel('Speleo');
			$this->loadModel('Canyon');
		}

		public function index(){
			$svg='
				<div class="svg">
					<svg class="scroll" viewBox="0 0 164 83" version="1.1" xmlns="http://www.w3.org/2000/svg" xml:space="preserve">
						<path class="path" d="M1,1l81,81l81,-81"/>
					</svg>
				</div>';
			$speleo=$this->Speleo->all();
			$canyon=$this->Canyon->all();
			$this->render('posts.index', compact('svg','speleo','canyon'));
		}

		public function equipe(){
			$this->description="De la passion au métier, c’est avec dynamisme et professionnalisme que je vous guide dans un monde naturel surprenant à travers le canyoning et la spéléologie.";
			$this->titre='Équipe | '.$this->titre;
			$this->render('posts.equipe');
		}

		public function mentions(){
			$this->indexation='<meta name="robots" content="noindex">';
			$this->titre='Mentions Légales | '.$this->titre;
			$this->render('posts.mentions');
		}

		public function charte(){
			$this->indexation='<meta name="robots" content="noindex">';
			$this->titre='Charte de qualité | '.$this->titre;
			$this->render('posts.charte');
		}

		public function cgv(){
			$this->indexation='<meta name="robots" content="noindex">';
			$this->titre='Conditions générales de vente | '.$this->titre;
			$this->render('posts.cgv');
		}

		public function contact(){
			$this->description="Un conseil, un renseignement ou une réservation, n’hésitez plus et rendez-vous sur le formulaire ou par téléphone.";
			$contact="";
			if (!empty($_POST)) {
				// RECAPTCHA -------------------------------------------------------
				function getCaptcha($SecretKey){
					$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".SECRET_KEY."&response={$SecretKey}");
					return json_decode($response);
				}
				$return = getCaptcha($_POST['g-recaptcha-response']);
				if($return->success == true && $return->score > 0.5){
					$contact= "success";
				// -----------------------------------------------------------------
					$prenom = $_POST['prenom'];
					$nom = $_POST['nom'];
					$email = $_POST['email'];
					$tel = $_POST['tel'];
					$message = $_POST['message'];
					$contenu= "Envoyé par : $nom $prenom<br>Téléphone: $tel<br>Message:<br>$message";
					$destinataire = "info@kalkeo.com";
					$sujet = "Formulaire de contact kalkeo.com";
					$mailheader = array(
						'From' => $email,
						'CC' => $email,
						'X-Mailer' => 'PHP/' . phpversion(),
						'X-Priority' => '1',
						'Mime-Version' => '1.0',
						'Content-type' => 'text/html; charset= utf-8',
						'charset' => 'UTF-8'
					);
					mail($destinataire, $sujet, $contenu, $mailheader) or die("Erreur!");
				}else{
					$contact= "robot";
				}
			}
			$this->titre='Contact | '.$this->titre;
			$this->render('posts.contact',compact('contact'));
		}
	}