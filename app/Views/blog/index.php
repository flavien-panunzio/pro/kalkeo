<main role="main" class="blog-index">
	<h1>Blog</h1>
	<?php $i=0;
		foreach ($blog as $value) :  $i++;
			if($i%2 == 1){
				$order1="order-1";
				$order2="order-2";
			}
			else{
				$order1="order-2";
				$order2="order-1";
		}?>
		<div class="row no-padding">
			<div class="col-md-4 images h-100 no-padding <?=$order1?>">
				<img class="lazy-load" src="/images/general/load.gif" data-src="<?=$value->image?>">
			</div>
			<div class="col-md-8 desc flex-center h-100 <?=$order2?>">
				<h2 class="no-padding"><?=$value->titre?></h2>
				<img class="d-none" src="<?=$value->image?>">
				<p><?=$value->getExtrait()?></p>
				<a class="btn btn-primary" href="/blog/<?=$value->id.'_'.$value->slug?>">Lire l'article</a>
			</div>
		</div>
	<?php endforeach; ?>
</main>