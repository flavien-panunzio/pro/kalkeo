<main role="main" class="blog-show">
	<a href="/blog">Revenir aux articles</a>
	<div class="d-flex">
		<img src="<?=$show->image?>">
		<div>
			<h1><?=$show->titre?></h1>
			<p><?=$show->contenu?></p>	
		</div>
	</div>
</main>