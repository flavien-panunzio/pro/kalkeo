<main role="main" class="lieux-show no-padding flex-center">
	<h1><?=$show->nom?></h1>
	<div class="row no-padding">
		<div class="col-lg-6 flex-center">
			<div class="description">
				<?=$show->description?>			
				<p class="lienmaps">Pour avoir l'itinéraire jusqu'au lieu exact, <a href="https://www.google.com/maps/dir/?api=1&destination=<?=$show->latitude2.",".$show->longitude2?>">cliquez ici</a> ou sur le marker de la carte ci-dessous.</p>
				<a href="/contact" class="btn btn-primary">Réserver</a>
			</div>
		</div>
		<div id="MainCarousel" class="carousel slide col-lg-6 flex-center" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#MainCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#MainCarousel" data-slide-to="1"></li>
				<li data-target="#MainCarousel" data-slide-to="2"></li>
				<li data-target="#MainCarousel" data-slide-to="3"></li>
			</ol>
			<div class="carousel-inner">
				<div class="carousel-item active">
					<a href="#carousel-modal"data-toggle="modal" data-slide-to="0">
						<img class="d-block w-100" src="/images/lieux/<?=$marker.'/'.$show->slug?>_1.jpg" alt="Photo <?=$show->nom?> 1"  onerror="this.src='/images/default-<?=$marker?>.jpg'">
					</a>
				</div>
				<div class="carousel-item">
					<a href="#carousel-modal"data-toggle="modal" data-slide-to="1">
						<img class="d-block w-100" src="/images/lieux/<?=$marker.'/'.$show->slug?>_2.jpg" alt="Photo <?=$show->nom?> 2"  onerror="this.src='/images/default-<?=$marker?>.jpg'">
					</a>
				</div>
				<div class="carousel-item">
					<a href="#carousel-modal"data-toggle="modal" data-slide-to="2">
						<img class="d-block w-100" src="/images/lieux/<?=$marker.'/'.$show->slug?>_3.jpg" alt="Photo <?=$show->nom?> 3"  onerror="this.src='/images/default-<?=$marker?>.jpg'">
					</a>
				</div>
				<div class="carousel-item">
					<a href="#carousel-modal"data-toggle="modal" data-slide-to="3">
						<img class="d-block w-100" src="/images/lieux/<?=$marker.'/'.$show->slug?>_4.jpg" alt="Photo <?=$show->nom?> 4"  onerror="this.src='/images/default-<?=$marker?>.jpg'">
					</a>
				</div>
			</div>
			<a class="carousel-control-prev" href="#MainCarousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#MainCarousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
	<div class="container-fluid row">
		<div class="js-marker d-none" data-lat="<?=$show->latitude?>" data-lng="<?=$show->longitude?>" data-link="show" data-marker="<?=$marker?>"></div>
		<div class="js-marker d-none" data-lat="<?=$show->latitude2?>" data-lng="<?=$show->longitude2?>" data-link="show" data-marker="<?=$marker?>-car"></div>
		<div class="map col-12" id="map" data-padding="100"></div>
	</div>
	<div class="grille container-fluid">
		<div class="tarifs">
			<h2 class="no-padding">Tarifs</h2>
			<h3 class="no-padding"><?=$show->tarif_solo?>€</h3>
			<div class="flex-center">
				<ul>
					<li><strong>Tarifs individuels : </strong><?=$show->tarif_solo?>€</li>
					<li><strong>Tarifs de groupes : </strong><?=$show->tarif_groupe?>€ (à partir de <?=$show->nb_groupe_min?> personnes)</li>
					<li>Pour des tarifs spéciaux (comités d'entreprise, collectivités, enterrement de vie de jeune fille/garçon...) : <a href="/contact">Me contacter</a></li>
				</ul>
				<p><a class="btn btn-primary" href="/contact">Réserver</a></p>
			</div>
		</div>
		<div class="ficheTech fiche flex-center">
			<h2>Fiche Technique</h2>
			<div>
				<p><strong>Niveau : </strong><?=$show->niveau?></p>
				<p><strong>Durée : </strong><?=$show->duree?></p>
				<p><strong>Temps d'approche : </strong><?=$show->temps_aller?> min</p>
				<p><strong>Temps de retour : </strong><?=$show->temps_retour?> min</p>
				<p><strong>Dénivelé : </strong><?=$show->denivele?> mètres</p>
				<p><strong>Age minimum : </strong><?=$show->age?> ans</p>
				<p><strong>Obstacles : </strong><?=$show->obstacles?></p>
				<p><strong>Communes : </strong><?=$show->commune?></p>
				<p><strong>Département : </strong><?=$show->departement?></p>
			</div>
		</div>
		<div class="matos fiche">
			<h2>Matériel à prévoir</h2>
			<?=$show->aprevoir?>
		</div>
		<div class="matos fiche">
			<h2>Le prix comprend</h2>
			<?=$show->compris?>
		</div>
	</div>
</main>

<!-- Modal -->
<div class="modal fade and carousel slide" id="carousel-modal" tabindex="-1" role="dialog" data-ride="carousel">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<ol class="carousel-indicators">
					<li data-target="#carousel-modal" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-modal" data-slide-to="1"></li>
					<li data-target="#carousel-modal" data-slide-to="2"></li>
					<li data-target="#carousel-modal" data-slide-to="3"></li>
				</ol>

				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block" src="/images/lieux/<?=$marker.'/'.$show->slug?>_1.jpg" alt="Photo <?=$show->nom?> 1" onerror="this.src='/images/default-<?=$marker?>.jpg'">
					</div>
					<div class="carousel-item">
						<img class="d-block" src="/images/lieux/<?=$marker.'/'.$show->slug?>_2.jpg" alt="Photo <?=$show->nom?> 2" onerror="this.src='/images/default-<?=$marker?>.jpg'">
					</div>
					<div class="carousel-item">
						<img class="d-block" src="/images/lieux/<?=$marker.'/'.$show->slug?>_3.jpg" alt="Photo <?=$show->nom?> 3" onerror="this.src='/images/default-<?=$marker?>.jpg'">
					</div>
					<div class="carousel-item">
						<img class="d-block" src="/images/lieux/<?=$marker.'/'.$show->slug?>_4.jpg" alt="Photo <?=$show->nom?> 4" onerror="this.src='/images/default-<?=$marker?>.jpg'">
					</div>
				</div>
				<a class="carousel-control-prev" href="#carousel-modal" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carousel-modal" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>