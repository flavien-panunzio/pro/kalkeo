<main role="main" class="lieux">
	<div class="entete">
		<?php if ($marker=="speleologie") : $text = "Spéléologie"; ?>
			<h1 style="background-image: url(/images/ban-speleo.jpg);">Spéléologie</h1>
			<div class="texte row no-padding">
				<div class="container">	
					<h2>La spéléologie en Rhône Alpes :</h2>
					<p>Le milieu souterrain attire et fascine les hommes, depuis la préhistoire. Pour découvrir ce milieu, il faut parfois parcourir des galeries, es rivières, escalader, descendre et remonter des puits, traverser des passages étroits, manipuler des cordes et des mousquetons, acquérir des techniques de progression en sécurité.</p><p>
					C’est pour cela que KalkeO vous propose des sorties adaptées au niveau de chacun, que ce soit pour les petits lutins en recherche d’aventures ou pour les sportifs désireux de vivre l’expérience d’une grande course. Notre équipe de moniteurs diplômés vous guidera dans un le milieu souterrain extraordinaire et vous enseignera différentes techniques de progression pour, pas à pas, aller vers l’autonomie.</p><p>
					Notre région offre un réel terrain de jeux, avec plus de 3000 grottes référencées seulement sur le Vercors. Vous avez déjà pratiqué quelques grottes par ici, ne vous inquiétez pas, avec ce large choix nous trouverons votre bonheur. C’est une activité parfois sportive, parfois ludique, parfois source de nombreux apprentissages mais surtout c’est le socle d’un solide esprit d’équipe.</p>
				</div>
			</div>
		<?php else : $text = "Canyoning"; ?>
			<h1 style="background-image: url(/images/ban-canyon.jpg);">Canyoning</h1>
			<div class="texte row no-padding">
				<div class="container">
					<h2>Se mettre au frais en Rhône Alpes :</h2>
					<p>Le canyoning un sport qui consiste à descendre des rivières avec des cascades, des vasques, des parties verticales… Cette activité est un moyen ludique pour « se mettre au frais ». Il faut faire appel parfois à la marche en terrain varié, la nage en eaux calmes ou vives, aux sauts, aux glissades, à la désescalade, à la descente en rappel et autres techniques d’évolution sur corde.</p>
					<p>Idéale pour un week-end entre amis ou durant une réunion de famille, pour les grands comme pour les petits. Notre région offre un panel intéressant de sites de pratiques. Au départ de Grenoble, de Valence ou de Chambéry, au croisement de l’Isère, de la Drôme ou de la Savoie, nous pourrons vous trouver un lieu adapté à votre demande.</p>
					<p>Une sortie découverte, pour gouter à la joie de vivre d’une première aventure en famille ou une sortie d’envergure, pour découvrir le côté sportif que nous réserve certains canyons. N’hésitez plus et faites appel à l’équipe de KalkeO pour un max de sécurité tout en gardant le côté ludique et fun de l’activité.</p>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<div class="filtres">
		<h2>Il y en a pour tous les goûts :</h2>
		<button class="btn btn-primary active" id="0">Tous les niveaux</button>
		<button class="btn btn-primary" id="1"><?=$text?> Découverte</button>
		<button class="btn btn-primary" id="2"><?=$text?> Sportif</button>
		<button class="btn btn-primary" id="3"><?=$text?> Grande Courses</button>
		<?php if ($marker=="speleologie") : ?>
			<button class="btn btn-primary" id="4">Bivouac souterrain</button>
		<?php endif; ?>
	</div>
	<div class="content-box">
		<div class="liste">
			<?php foreach ($index as  $value) :
				if($value->visible):
					if (strlen(strip_tags(explode('</p>', $value->description)[0]))>=130){
						$description=substr(strip_tags(explode('</p>', $value->description)[0]),0,130)."...";
					}else {
						$description=strip_tags(explode('</p>', $value->description)[0]);
					}?>
				<div class="item js-marker niveau<?=$value->niveau?>" data-lvl="<?=$value->niveau?>" data-lat="<?=$value->latitude?>" data-lng="<?=$value->longitude?>" data-link="<?=$marker.'/'.$value->id.'_'.$value->slug?>" data-marker="<?=$marker?>">
					<a href="/<?=$marker?>/<?=$value->id.'_'.$value->slug?>">
						<img class="lazy-load" src="/images/general/load.gif" onerror="this.src='/images/default-<?=$marker?>.jpg'" data-src="/images/lieux/<?=$marker.'/'.$value->slug?>_1.jpg" alt="Photo de présentation <?=$value->nom?>">
					</a>
					<div class="flex-center">
						<h3><?=$value->nom?></h3>
							<p><?=$description?></p>
						<a class="btn btn-primary" href="/<?=$marker?>/<?=$value->id.'_'.$value->slug?>">En savoir plus</a>
					</div>
				</div>
			<?php
				endif;
			endforeach; ?>
		</div>
		<div class="map" id="map" data-padding="40"></div>
	</div>
</main>