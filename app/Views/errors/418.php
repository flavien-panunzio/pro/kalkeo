<?php header('HTTP/1.0 418 I\'m a teapot');?>
<main class="error teapot flex-center" title="Infinitea : Sheepfilms.co.uk">
	<img src="/images/general/teapot.svg">
	<h2>Je suis une théière</h2>
	<a class="btn btn-primary" href="/">Revenir à l'accueil</a>
</main>