<?php header('HTTP/1.0 302 Found');?>
<main class="error maintenance flex-center">
	<img src="/images/general/maintenance.svg" alt="Photo by Ilya Pavlov on Unsplash">
	<h2>Cette page est en maintenance, veuillez nous excusez.</h2>
	<a class="btn btn-primary" href="/">Revenir à l'accueil</a>
</main>