<div class="wrapper">
	<nav id="sidebar">
		<div>
			<div class="sidebar-header">
				<a href="/"><img src="/images/logo.png" alt="logo KalkeO"></a>
			</div>
			<ul class="liens">
				<li>
					<a href="/">Accueil</a>
				</li>
				<li>
					<a href="/speleologie">Spéléologie</a>
				</li>
				<li>
					<a href="/canyoning">Canyoning</a>
				</li>
				<li>
					<a href="/equipe">L'équipe</a>
				</li>
				<li>
					<a href="/blog">Blog</a>
				</li>
				<li>
					<a href="/contact">Contact</a>
				</li>
			</ul>
		</div>
		<div>
			<ul class="contact">
				<li>Me Contacter</li>
				<li>Manon ROCHE</li>
				<li>
					<a href="tel:0630992929">06.30.99.29.29</a>
				</li>
				<li>
					<a href="mailto:info@kalkeo.com">info@kalkeo.com</a>
				</li>
			</ul>
			<div class="reseaux">
				<a href="https://www.facebook.com/speleocanyonkalkeO/"><i class="fab fa-facebook-square fa-2x"></i></a>
				<a href="https://fr.tripadvisor.ca/Attraction_Review-g6636944-d15023812-Reviews-KalkeO-Le_Versoud_Isere_Auvergne_Rhone_Alpes.html"><i class="fab fa-tripadvisor fa-2x"></i></a>
				<a href="https://www.instagram.com/kalkeofficial"><i class="fab fa-instagram fa-2x"></i></a>
			</div>
		</div>
	</nav>
	<div id="content">
		<header id="header-landing">
			<nav class="navbar">
				<a class="navbar-brand" href="/"><img src="/images/logo.png" alt="logo KalkeO"></a>
				<div id="sidebarCollapse"><div class="burger"></div></div>
			</nav>
		</header>
		<div id="fullpage" class="index">
			<div class="section s1" id="section1">
				<ul class="carousel">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
				<div></div><div></div>
				<div class="titres">
					<h1>KALKEO</h1>
					<h2>SPORT NATURE EN RÉGION GRENOBLOISE</h2>
				</div>
				<div class="bouttons">
					<a class="btn btn-primary" href="/speleologie">Spéléologie</a>
					<a class="btn btn-primary" href="/canyoning">Canyoning</a>	
				</div>
				<a href="#kalkeo">
					<?=$svg?>
				</a>
			</div>
			<div class="section s2" id="section2">
				<div class="maps">
					<div class="flex-center text">
						<h3>Bienvenue sur le site de KalkeO,</h3>
						<p>C’est une structure crée en 2017 à Grenoble dans le but de vous faire découvrir notre environnement naturel par le biais de la spéléologie et du canyoning.</p>
						<p>Vous êtes débutant ou pratiquant régulier, jeune ou moins jeune, en famille ou entre amis, vous êtres au bon endroit pour partager de belles aventures.</p>
						<p>KalkeO propose l’encadrement et l’organisation des activités sportives de pleine nature et aussi l’enseignement pour petit à petit aller vers l’autonomie.</p>
						<p>Au départ de Grenoble, Valence, Chambéry, l’équipe de professionnels de KalkeO vous emmène arpenter les massifs du Vercors, de la Chartreuse et des Bauges.</p>
					</div>
					<div class="liste">
						<?php foreach ($speleo as  $value) :?>
							<?php if($value->visible){ ?>
							<div class="item js-marker" data-lat="<?=$value->latitude?>" data-lng="<?=$value->longitude?>" data-txt="<?=$value->nom?>" data-link="speleologie/<?=$value->id.'_'.$value->slug?>" data-marker="speleologie">
							</div>
							<?php } ?>
						<?php endforeach; ?>
						<?php foreach ($canyon as  $value) :?>
							<?php if($value->visible){ ?>
							<div class="item js-marker" data-lat="<?=$value->latitude?>" data-lng="<?=$value->longitude?>" data-txt="<?=$value->nom?>" data-link="canyoning/<?=$value->id.'_'.$value->slug?>" data-marker="canyoning">
							</div>
							<?php } ?>
						<?php endforeach; ?>
					</div>
					<div class="map" id="map" data-padding="40"></div>
				</div>
				<a href="#speleologie">
					<?=$svg?>
				</a>
			</div>
			<div class="section s3" id="section3">
				<div class="flex-center droite presentation">
					<label>Traversée de la dent de Crolles</label><hr>
					<h2>Spéléologie</h2>
					<p>La spéléologie une activité fascinante au cœur de la terre.</p>
				</div>
				<div class="flex-center bleu gauche">
					<h3>à la découverte du monde souterrain</h3>
					<p>Basé à Grenoble, KalkeO se trouve au croisement de trois massifs calcaires importants. Une aubaine pour la pratique de la spéléologie. Ce sol calcaire qui subit une érosion forte depuis la nuit des temps, nous offre aujourd’hui l’accès à une multitude de grottes aux alentours de Grenoble. N’hésitez plus, venez découvrir les secrets du milieu souterrain. C’est une activité passionnante qui mérite d’être abordée sans préjugés.</p>
					<a class="btn-default" href="/speleologie">En savoir plus</a>
				</div>
				<a href="#canyoning">
					<?=$svg?>
				</a>
			</div>
			<div class="section s4" id="section4">
				<div class="flex-center gauche presentation">
					<label>Canyon du Versoud</label><hr>
					<h2>canyoning</h2>
					<p>Le canyoning une activité ludique dans un milieu naturel magnifique.</p>
				</div>
				<div class="flex-center bleu droite">
					<h3>Allez on se jette à l’eau !</h3>
					<p>Au départ de Grenoble, Valence et Chambery, KalkeO vous propose de nombreux parcours adaptés à votre demande. La nature fait bien les choses, avec du temps, beaucoup de temps, la roche, sculptée par l’eau, nous offre un terrain de jeu exceptionnel entre toboggans naturels, vasques d’eaux turquoise et cascades vous ne saurez plus où donner de la tête. Jetez-vous à l’eau, contactez-nous !!</p>
					<a class="btn-default" href="/canyoning">En savoir plus</a>
				</div>
				<div></div>
			</div>
		</div>
	</div>
</div>