<main class="equipe">
	<h1>L'équipe</h1>
	<div class="row no-padding">
		<div class="col-lg-8">
			<div>
				<p>Manon ROCHE<br>
				Gérante de KalkeO<br>
				Monitrice diplômée :
				</p>
				<ul>
					<li>DEJEPS canyon</li>
					<li>DEJEPS spéléologie</li>
					<li>Educatrice spécialisée</li>
				</ul>
			</div>
			<p><i>« Depuis le plus jeune âge, j’ai été sensibilisé au milieu naturel, c’est donc tout naturellement que je me suis tournée vers les sports de nature. Après être passée par le lycée Sport Nature du Diois, je me suis décidée à faire de ma passion, mon métier. Aujourd’hui, j’ai envie de vous faire découvrir la spéléologie et le canyoning et de vous sensibiliser à notre environnement parfois si fragile. Il est primordial pour moi de vivre ces moments dans un esprit de préservation du milieu, respect de la faune et de la flore, de favoriser le covoiturage, de ramasser les déchets, si nécessaire, pour espérer profiter le plus longtemps de ces sites naturels exceptionnels.</i></p>
			<p><i>Également titulaire d’un Diplôme d’Etat d’Educateur Spécialisé, j’aime le côté humain de nos activités et je reste convaincue que les sports de nature véhiculent des valeurs importantes dans notre société actuelle : environnementales, éducatives, sociales…</i></p>
			<p><i>Si vous voulez découvrir les sports de nature et surtout partager de belles aventures, n’hésitez pas à contact kalkeO, je me ferais un plaisir de répondre à votre demande.»</i></p>
			<a target="_blank" href="/download/diplome-manon_roche.pdf">Télécharger les documents professionels</a>
		</div>
		<div class="col-lg-4 flex-center">
			<img class="lazy-load" src="/images/general/load.gif" data-src="/images/manon.jpg">
		</div>
	</div>
</main>