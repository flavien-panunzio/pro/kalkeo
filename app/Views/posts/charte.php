<div class="mentions container">
	<h1>Charte de qualité</h1>
	<p>
		<ul>
			<p>KalkeO vous propose des activités de pleine nature et plus particulièrement des sorties canyoning et spéléologie dans la région Grenobloise sur le massif du Vercors, de la chartreuse et des Bauges mais nous pouvons également vous organiser des séjours sur d’autres départements (ex : Alpes maritimes, Pyrénées…). Nous nous engageons à penser au mieux vos sorties qu’elles soient en lien avec vos attentes mais également de manière qu’elles se déroulent en toute sécurité : 
				<ul>
					<li>Choix du nombre de personne en fonction du lieu de pratique</li>
					<li>Organisation de la sortie ; prise en compte de l’aspect météorologique et du potentiel de chacun</li>
					<li>Matériel de qualité</li>
					<li>Ecoute</li>
				</ul>
			</p>
			<p>KalkeO s’engage à respecter au mieux l’environnement naturel si important dans nos activités de pleine nature. Ainsi, nous favorisons les sites de proximité, le covoiturage, nous participons également à des campagnes de dépollution.</p>
			<p>KalkeO s’engage à faire appel à des moniteurs diplômés d’état garanties en responsabilité civile professionnelle pour les activités qu’ils encadrent. Il est conseillé d’avoir également une assurance responsabilité individuel (comprise parfois dans l’assurance habitation ou du véhicule) pour pratiquer ces activités.</p>
			<p>Nous sommes aussi membre du <a href="http://www.syndicat-speleo-canyon.org/syndicat/charte-de-qualite">SNPSC (Syndicat National des Professionnels de la Spéléologie et du Canyon)</a> qui possède une charte de qualité bien définie pour nos activités et nous partageons ces valeurs.</p>
			<p>KalkeO promeut une pratique qualitative des activités dans un souci de partage des connaissances et de respect de l’environnement naturel.</p>
			<p>KalkeO s’engage à mettre à disposition de ses clients du matériel spécifique de qualité tant pour le confort que pour un aspect sécuritaire. Le matériel technique est soumis à une vérification régulière et possède un suivi EPI (Equipement de Protection Individuel).</p>
		</ul>
	</p>
</div>