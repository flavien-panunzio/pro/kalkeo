<div class="mentions container">
	<h1>Conditions générales de vente</h1>
	<h2>Inscription et réservation :</h2>
	<ul>
		<li>Par mail : <a href="mailto:info@kalkeo.com">info@kalkeo.com</a></li>
		<li>Par téléphone : <a href="tel:0630992929">0630992929</a></li>
	</ul>
	
	<h2>Conditions de participation pour la pratique du canyoning et de la spéléologie :</h2>
	<ul>
		<li>Savoir nager</li>
		<li>Être en pleine possession de ses moyens : ne pas être en état d’ébriété ou sous l’emprise de stupéfiants</li>
		<li>Ne pas présenter de contre-indication au sport</li>
		<li>Ne pas avoir subit d’opération récente ou être blessé</li>
	</ul>
	<p>Chaque personne doit signaler au préalable toutes pathologies ou antécédents liés à : difficultés respirations, asthme, diabète, blessures, fractures antérieures…</p>
	<p>Le client s’engage à respecter les consignes de sécurité données par le moniteur, ce dernier se réserve le droit d’exclure toute personne mettant en danger sa vie ou celle du groupe. En cas de non-respect du règlement et de l’exclusion d’une personne aucun remboursement est effectué.</p>
	<p>Les enfants doivent être accompagnés ou avoir une autorisation parentale.</p>
	
	<h2>Tarification et modalité :</h2>
	<p>Les tarifs donnés sur le site <a href="https://www.kalkeo.com/">https://www.kalkeo.com</a> sont en euros TTC. 
	Chaque tarif comprend ; un encadrement par un moniteur diplômé, du matériel de qualité, les photos en format numérique et une assurance de responsabilité civile.</p>
	<p>Pour toute réservation effectuer plus de 30 jours avant la date de prestation, KalkeO vous demande un acompte de 50% et d’avoir pris connaissances des conditions générales de vente.</p>
	<p>Le règlement peut être fait sur place en espèces ou en chèques.</p>
	<h2>Assurance :</h2>
	<p>Les moniteurs de KalkeO sont des professionnels diplômés d'Etat agréés Jeunesse & Sport qui remplissent toutes les obligations légales en vigueur. Ils possèdent une assurance Responsabilité Civile Professionnelle* couvrant l'encadrement des activités réservées. Sur demande du client, ils doivent fournir toutes les pièces justificatives nécessaires.</p>
	<p>* En cas d'accident, le moniteur dispose d'une RC Pro si une faute professionnelle se trouve avérée. Toutefois, le moniteur n'est pas responsable en cas de non-respect des consignes données. En outre, il est conseillé à nos clients de se renseigner à propos d'une assurance Individuelle Accident valable pour des accidents liés à la pratique d'activités de pleine nature (voir avec assurance habitation, véhicule, ou carte de crédit).</p>
	<h2>Annulation :</h2>
	<p>Pour toute annulation de la part du client :</p>
	<ul>
		<li>le solde versé est remboursé dans sa totalité si KalkeO est informé de l’annulation au moins 21 jours avant la date de l’activité.</li>
		<li>le solde versé est remboursé à 50 % si KalkeO est informé de l’annulation au moins 10 jours avant la date de l’activité.</li>
		<li>le solde versé n’est pas remboursé si KalkeO est informé de l’annulation moins 10 jours avant la date de l’activité.</li>
	</ul>
	<p>Toute annulation partielle ou totale du client, due à un cas de force majeure donnera lieu au remboursement des sommes versées, ainsi qu’au non-paiement de la prestation. Le client devra alors fournir un justificatif (certificat médical ou acte officiel).</p>
	<p>KalkeO se réserve le droit d’annuler une sortie si le nombre minimal de participation n’est pas atteint (4 personnes) sauf tarifs spéciaux et si la météo ne permet pas une sortie en toute sécurité.</p>
	<h2>Retard :</h2>
	<p>Une heure de rendez-vous est systématiquement communiquée lors de l'inscription. Pour le bon déroulement des activités et par respect envers les autres participants, nous tolérons un retard de 20 minutes maximum. Au-delà de ce délai, le moniteur peut partir en activité et aucun remboursement ou indemnité sera exigible.</p>
	<h2>Droit à l’image :</h2>
	<p>En participant aux activités organisées par KalkeO vous acceptez que votre image soit utilisée à des fins promotionnelles et publicitaires sur tous nos supports de communication (flyer, carte de visite, affiche, autocollant, publicité sur véhicule, site internet, blog…), sauf demande écrite de votre part, adressée par lettre recommandée avec accusé de réception.</p>
	<p>Conformément à la Loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, vous disposez d’un droit d’accès et de rectification ou suppression des informations vous concernant.</p>
	<h2>Vols/Dégradations :</h2>
	<p>Le client est averti qu'il est fortement déconseillé d'emmener et de laisser des objets de valeur dans un véhicule lors de activités. Les moniteurs de KalkeO ne sont en aucun cas responsables, ni préjudiciables en cas de perte, vol, ou dégradation.</p>
</div>