<main class="contact container-fluid">
	<h1>Contact</h1>
	<div class="row">
		<div class="col-lg-6">
			<div class="flex-center">
				<h2>Mes coordonnées</h2>
				<p>Manon ROCHE</p>
				<p><a href="tel:0630992929">06.30.99.29.29</a></p>
				<p><a href="mailto:info@kalkeo.com">info@kalkeo.com</a></p>
			</div>
			<div class="flex-center">
				<h2>Carte cadeau KalkeO</h2>
				<img src="/images/cadeau.jpg" alt="Carte cadeau KalkeO">
			</div>
		</div>
		<div class="col-lg-6">
			<h2>Formulaire de contact</h2>
			<div class="flex-center">
				<form method="post">
					<?php if ($contact=="robot") :?>
						<div class="alert alert-danger" role="alert">
							Le reCAPTCHA n'a pas été validé : vous êtes un robot !
						</div>
					<?php elseif ($contact=="success") :?>
						<div class="alert alert-primary" role="alert">
							Votre message a bien été envoyé, je vous répondrais dans les plus brefs délais.
						</div>
					<?php endif; ?>
					<div class="form-row">
						<div class="col-md-6">
							<label for="prenom">Nom</label>
							<input type="text" name="prenom" class="form-control" id="prenom" required>
						</div>
						<div class="col-md-6">
							<label for="nom">Prénom</label>
							<input type="text" name="nom" class="form-control" id="nom" required>
						</div>	
					</div>
					<div class="form-row">
						<div class="col-md-6">
							<label for="email">E-mail</label>
							<input type="email" name="email" class="form-control" id="email" required>
						</div>
						<div class="col-md-6">
							<label for="tel">Téléphone</label>
							<input type="tel" name="tel" class="form-control" id="tel" required>
						</div>
					</div>
					<div class="form-group">
							<label for="message">Message</label>
							<textarea rows="4" cols="20" name="message" class="form-control" id="message" required></textarea>
					</div>
					<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response" required>
					<button type="submit" class="btn btn-primary submit">Envoyer</button>
				</form>
			</div>
		</div>
	</div>
</main>

<!-- RECAPTCHA ---------------------------------->
<script src="https://www.google.com/recaptcha/api.js?render=<?=SITE_KEY?>"></script>
<script>
	grecaptcha.ready(function() {
		grecaptcha.execute('<?=SITE_KEY?>', {action: 'homepage'}).then(function(token) {
			document.getElementById('g-recaptcha-response').value=token;
		});
	});
</script>