<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<meta name="author" content="Panunzio Flavien" />
		<meta name="copyright" content="© 2018 Flavien Panunzio" />

		<meta name="title" content="<?=$this->titre?>" />
		<title><?=$this->titre?></title>

		<link rel="icon" href="/images/favicon.png" type="image/x-icon"/>

		<!-- CSS -->
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="/style/admin/css/style.css">
		
		<!-- JAVASCRIPT -->
		<script src="/include/jquery.min.js"></script>
		<script src="/include/popper.min.js"></script>
		<script src="/include/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/admin.js"></script>
		<script type="text/javascript" src="/js/upload.js"></script>
	</head>
	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
				<a class="navbar-brand" href="/admin/index">KalkeO</a>
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="/admin/index">Accueil</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/admin/lieux">Lieux</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/admin/blog">Blog</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/admin/equipe">Equipe</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/admin/media">Média</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/logout">Logout</a>
					</li>
				</ul>
			</nav>
		</header>
		<main role="main" class="container">
			<?= $content; ?>
		</main>
	</body>
</html>