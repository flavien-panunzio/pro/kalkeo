<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?=$this->indexation?>

		<meta name="author" content="Panunzio Flavien" />
		<meta name="copyright" content="© 2018 Flavien Panunzio" />

		<!-- COULEUR BAR URL IPHONE ANDROID WINDOWS-PHONE -->
		<meta name="apple-mobile-web-app-status-bar-style" content="#5073B4" />
		<meta name="theme-color" content="#5073B4" />
		<meta name="msapplication-navbutton-color" content="#5073B4">

		<!-- META OpenGraph -->
		<meta property="og:title" content="<?=$this->titre?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" />
		<meta property="og:description" content="<?=$this->description?>" />
		<meta property="og:image" content="https://<?=$_SERVER['HTTP_HOST']?>/images/landing/carousel4.jpg" />
		
		<title><?=$this->titre?></title>
		<meta name="description" content="<?=$this->description?>" />
		<meta name="keywords" content="canyoning, speleologie, sport, grenoble, activitée sportive, tourisme, découverte, apprentissage, sortie">

		<link rel="icon" href="/images/favicon.png" type="image/x-icon"/>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<meta name="google-site-verification" content="yZHQaIkYxrlnbCfz6hou9te8wT-2vl8tj4r3iUygH-M" />
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133479314-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-133479314-1');
		</script>

		<!-- Lazy Load (chargement des images) -->
		<script src="/js/lazy-load.js"></script>

		<!-- CSS -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="/style/default/css/style.css">

		<!-- JS -->
		<script src="/include/jquery.min.js"></script>
		<script src="/include/popper.min.js"></script>
		<script src="/include/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/script.js"></script>
	</head>
	<body>
		<div class="load load-show flex-center"><i class="fas fa-spinner fa-4x fa-spin"></i></div>		
		<?php if ($_SERVER['REQUEST_URI']==='/') :
			echo $content;
		else: ?>
			<nav class="navbar navbar-expand-lg bg-secondary sticky-top">
				<a class="navbar-brand" href="/"><img src="/images/logo.png" alt="logo Kalkeo"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<div class="burger"></div>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link" href="/">Accueil</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/speleologie">Spéléologie</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/canyoning">Canyoning</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/equipe">L'équipe</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/blog">Blog</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/contact">Contact</a>
						</li>
					</ul>
					<div class="infos-nav">
						<p>Manon ROCHE</p>
						<p><a href="tel:0630992929"><i class="fas fa-phone"></i> 06.30.99.29.29</a></p>
						<p class="no-padding"><a href="mailto:info@kalkeo.com"><i class="fas fa-envelope"></i> info@kalkeo.com</a></p>
					</div>
				</div>
			</nav>
			<?= $content; ?>
			<footer role="contentinfo">
				<div class="container">
					<div class="row no-padding">
						<div class="col-md-4 flex-center">
							<h3><i class="fas fa-map-marker-alt"></i> Nous trouver</h3>
							<p>100, Rue du Gresivaudan</p>
							<p>38420 Le Versoud</p>
							<p><a href="tel:0630992929"><i class="fas fa-phone fa-2x align-middle"></i> 06.30.99.29.29</a></p>
							<p><a href="mailto:info@kalkeo.com"><i class="fas fa-envelope fa-2x align-middle"></i> info@kalkeo.com</a></p>
						</div>
						<div class="col-md-4 flex-center">
							<img class="w-50" src="/images/logo2.png" alt="Logo Kalkeo">
						</div>
						<div class="col-md-4 flex-center">
							<h3><i class="fas fa-link"></i> Sitemap</h3>
							<p class="text-center"><a href="/speleologie">Spéléologie</a> | <a href="/canyoning">Canyoning</a> | <a href="/equipe">L'équipe</a> | <a href="/blog">Blog</a> | <a href="/contact">Contact</a></p>
							<h3><i class="fas fa-comments"></i> Suivez-nous</h3>
							<div class="reseaux">
								<a href="https://www.facebook.com/speleocanyonkalkeO/"><i class="fab fa-facebook-square fa-4x"></i></a>
								<a href="https://fr.tripadvisor.ca/Attraction_Review-g6636944-d15023812-Reviews-KalkeO-Le_Versoud_Isere_Auvergne_Rhone_Alpes.html"><i class="fab fa-tripadvisor fa-4x"></i></a>
								<a href="https://www.instagram.com/kalkeofficial"><i class="fab fa-instagram fa-4x"></i></a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-4 text-center"><a href="/charte-de-qualite">Charte de qualité</a></div>
						<div class="col-4 text-center"><a href="/cgv">Contitions générales de vente</a></div>
						<div class="col-4 text-center"><a href="/mentions-legales">Mentions légales</a></div>
					</div>
					<div class="row flex-center text-center">
						<p class="text-center">Site réalisé avec ❤️ par <a href="https://latoile.dev" target="_blank" rel="noopener">LaToile.dev</a> | &copy <?= date("Y"); ?> Kalkeo</p>
					</div>
				</div>
			</footer>
		<?php endif; ?>
	</body>

	<!-- COOKIES -->
	<script type="text/javascript" id="cookieinfo" src="//cookieinfoscript.com/js/cookieinfo.min.js"
		data-message ="Nous utilisons des cookies pour améliorer votre expérience. En continuant à visiter ce site, vous acceptez notre utilisation des cookies."
		data-linkmsg ="Plus d'infos">
	</script>
</html>