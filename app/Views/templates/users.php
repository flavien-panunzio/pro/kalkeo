<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?=$this->indexation?>

		<meta name="author" content="Panunzio Flavien" />
		<meta name="copyright" content="© 2018 Flavien Panunzio" />

		<meta name="title" content="Connexion | <?=$this->titre?>" />
		<title>Connexion | <?=$this->titre?></title>

		<link rel="icon" href="/images/favicon.png" type="image/x-icon"/>

		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
	</head>
	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
				<a class="navbar-brand" href="#">Connexion admin</a>
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="/">Revenir au site</a>
					</li>
				</ul>
			</nav>
		</header>
		<main role="main" class="container">
			<div class="starter-template">
				<?= $content; ?>
			</div>
		</main>
	</body>
</html>