<h1>Gestion des lieux de spéléologie et de canyoning</h1>
<div class="d-flex align-items-center justify-content-between">
	<div><a href="/admin/ajoutlieux/speleo" class="btn btn-success">Ajouter un lieu de spéléologie</a></div>
	<div class="filtres">
		<button class="btn btn-primary" id="all">Tout</button>
		<button class="btn btn-primary" id="speleo">Spéléologie</button>
		<button class="btn btn-primary" id="canyon">Canyoning</button>
	</div>
	<div><a href="/admin/ajoutlieux/canyon" class="btn btn-success">Ajouter un lieu de canyoning</a></div>
</div>
<table class="table">
	<thead>
		<tr>
			<td>Id</td>
			<td>Titre</td>
			<td>Actions</td>
			<td>Visible</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($index as $post) :
			if ($post->visible) $visible = "checked";
			else $visible="";
		?>
			<tr class="item type<?=$post->type ?>">
				<td><?= $post->id;?></td>
				<td><?= $post->nom;?></td>
				<td>
					<a class="btn btn-primary" href="/admin/modiflieux/<?= $post->type."_".$post->id; ?>">Éditer</a>
					<a class="btn btn-danger" href="/admin/supprlieux/<?= $post->type."_".$post->id; ?>">Supprimer</a>
				</td>
				<td><form><input type="checkbox" name="visible" <?=$visible?> data-table="<?=$post->type?>" data-id="<?=$post->id?>"></form></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<script>
	$(".filtres .btn").click(function() {
		filtre=$(this).attr('id');
		if (filtre =='all') {
			$(".filtres .btn").removeClass('active');
			$(this).addClass('active');
			$(".item").removeClass('d-none');
		}
		else{
			$(".filtres .btn").removeClass('active');
			$(this).addClass('active');
			$(".item").addClass("d-none");
			$(".type"+filtre).removeClass("d-none");
		}
	});

	$('form input').change(function(event) {
		if ($(this).is(':checked')) {
			value=1;
		}else{
			value=0;
		}
		id=$(this).data('id')+"_"+$(this).data('table')+"_"+value;
		$.post('/admin/visible', {id : id});
	});
</script>