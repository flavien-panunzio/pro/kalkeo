<?php if($table == "Speleo") $table="speleologie"; else $table="canyoning";?>
<form method="post">
	<div class="d-flex justify-content-between">
		<div>
			<a class="btn btn-primary" href="/admin/lieux">Retour</a>
		</div>
		<div class="flex-center">
			<h1>Modification des lieux</h1>
			<a class="btn btn-primary" target="_blank" href="/<?=$table?>/<?=$post->id.'_'.$post->slug?>">Voir le lieu</a>
		</div>
		<div>
			<button type="submit" class="btn btn-success">Sauvegarder</button>
		</div>
	</div>
	<h2>Informations</h2>
	<div class="form-row lieux-edit">
		<div class="form-group col-12">
			<label>Nom du lieux</label>
			<input type="text" name="nom" value="<?=$post->nom?>" required>
		</div>
		<div class="col-2 d-flex align-items-center justify-content-around">
			<label>Slug-auto</label>
			<input id="slug-auto" type="checkbox" checked>
		</div>
		<div class="form-group col-10">
			<label>Slug</label>
			<input type="text" name="slug" value="<?=$post->slug?>" required readonly="true" ondblclick="this.readOnly='';" onfocusout="this.readOnly='true';">
		</div>
		<div class="form-group col-md-6">
			<label>Latitude</label>
			<input type="number" step="any" name="latitude" value="<?=$post->latitude?>">
		</div>
		<div class="form-group col-md-6">
			<label>Longitude</label>
			<input type="number" step="any" name="longitude" value="<?=$post->longitude?>">
		</div>
		<div class="form-group col-md-6">
			<label>Latitude 2</label>
			<input type="number" step="any" name="latitude2" value="<?=$post->latitude2?>">
		</div>
		<div class="form-group col-md-6">
			<label>Longitude 2</label>
			<input type="number" step="any" name="longitude2" value="<?=$post->longitude2?>">
		</div>
		<div class="form-group col-12">
			<label>Description</label>
			<textarea class="summernote summernote-description" name="description"><?=$post->description?></textarea>
		</div>
		<div class="form-group col-md-4">
			<label>Niveau</label>
			<input type="number" name="niveau" value="<?=$post->niveau?>">
		</div>
		<div class="form-group col-md-4">
			<label>Durée</label>
			<input type="text" name="duree" value="<?=$post->duree?>">
		</div>
		<div class="form-group col-md-4">
			<label>Âge</label>
			<input type="number" name="age" value="<?=$post->age?>">
		</div>
		<div class="form-group col-md-4">
			<label>Tarif individuel</label>
			<input type="number" name="tarif_solo" value="<?=$post->tarif_solo?>">
		</div>
		<div class="form-group col-md-4">
			<label>Tarif groupe</label>
			<input type="number" name="tarif_groupe" value="<?=$post->tarif_groupe?>">
		</div>
		<div class="form-group col-md-4">
			<label>Nombre mini tarif groupe</label>
			<input type="number" name="nb_groupe_min" value="<?=$post->nb_groupe_min?>">
		</div>
		<div class="form-group col-md-4">
			<label>Temps aller</label>
			<input type="number" name="temps_aller" value="<?=$post->temps_aller?>">
		</div>
		<div class="form-group col-md-4">
			<label>Temps retour</label>
			<input type="number" name="temps_retour" value="<?=$post->temps_retour?>">
		</div>
		<div class="form-group col-md-4">
			<label>Dénivelé</label>
			<input type="number" name="denivele" value="<?=$post->denivele?>">
		</div>
		<div class="form-group col-md-4">
			<label>Obstacles</label>
			<input type="text" name="obstacles" value="<?=$post->obstacles?>">
		</div>
		<div class="form-group col-md-4">
			<label>Département</label>
			<input type="text" name="departement" value="<?=$post->departement?>">
		</div>
		<div class="form-group col-md-4">
			<label>Communes</label>
			<input type="text" name="commune" value="<?=$post->commune?>">
		</div>
		<div class="form-group col-6">
			<label>Matéreil à prévoir</label>
			<textarea class="summernote summernote-matos" name="aprevoir"><?=$post->aprevoir?></textarea>
		</div>
		<div class="form-group col-6">
			<label>Matériel compris</label>
			<textarea class="summernote summernote-matos" name="compris"><?=$post->compris?></textarea>
		</div>
	</div>
</form>
<h2>Images</h2>


<div class="upload-images lieux-images no-padding">
	<?php for ($i=1; $i <= 4; $i++) : ?>
		<div>
			<img onerror="this.src='/images/default.png'" src="/images/lieux/<?=$table."/".$post->slug."_".$i?>.jpg" data-nom="<?=$post->slug."_".$i?>.jpg" data-folder="images/lieux/<?=$table."/"?>">
		</div>
	<?php endfor; ?>
</div>
<?php require ROOT . '/app/Views/admin/media/modal.php'; ?>

<script type="text/javascript">
	//AUTO SLUG ------------------------------------------------------------------
	$('input[name="nom"]').on("input", function() {
		console.log($("#slug-auto").val());
		if($("#slug-auto").is(":checked")){
			value = $(this).val().toLowerCase().replace("à","a").replace(/é|ê|è/g,"e").replace("ç","c").replace("&","et").replace("@","a").replace("²","2").replace(/[^a-z0-9-]/g, "_");
			$('input#slug').val(value);
		}
	});

	//AUTO SAVE ------------------------------------------------------------------
	setInterval(function(){
		var result = [];
		$("input.form-control, textarea, select.form-control").each(function() {
			if($(this).attr('name') != undefined)
				result.push([$(this).attr('name')] +'|||'+ $(this).val());
		});
		select = window.location.href.split("/")[window.location.href.split("/").length-1].split("_");
		$.post('admin.lieux.edit', {
			ajax: true,
			id:select[1],
			table:select[0],
			data : result
		});
	}, 20000);
</script>