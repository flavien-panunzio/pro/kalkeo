<a class="btn btn-primary" href="/admin/blog">Retour</a>
<form method="post">
	<div class="form-group">
		<label>Titre de l'article</label>
		<input type="text" class="form-control" name="titre" value="<?=$post->titre?>">
	</div>
	<div class="form-group">
		<label>Slug</label>
		<input type="text" name="slug" value="<?=$post->slug?>">
	</div>
	<div class="form-group">
		<label>Contenu</label>
		<textarea class="summernote summernote-blog" name="contenu" rows="3"><?=$post->contenu?></textarea>
	</div>
	<div class="form-group">
		<label>Image</label>
		<input type="text" name="image" value="<?=$post->image?>">
	</div>

	<button type="submit" class="btn btn-primary">Sauvegarder</button>
</form>