<h1>Administrer les articles</h1>
<p><a href="/admin/ajoutblog" class="btn btn-success">Ajouter</a></p>
<table class="table">
	<thead>
		<tr>
			<td>Id</td>
			<td>Titre</td>
			<td>Actions</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($posts as $post) : ?>
			<tr>
				<td><?= $post->id;?></td>
				<td><?= $post->titre;?></td>
				<td>
					<a class="btn btn-primary" href="/admin/modifblog/<?=$post->id?>">Éditer</a>
					<button class="btn btn-danger" data-href="/admin/supprblog/<?=$post->id?>">Supprimer</button>
				</td>
				
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>