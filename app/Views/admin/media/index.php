<?php if($_GET["id"]!="/") : ?>
	<a class="btn btn-primary" href="/admin/media" role="button">Retour</a>
<?php endif; 
if($files!=null) : ?>
	<div class="tuiles">
		<?php foreach ($files as $value) :
			if(strstr($value,'.')): ?>
				<div class="upload-images">
					<img src="/images<?=$folder.$value;?>" data-nom="<?=$value?>" data-folder="images<?=$folder?>">
				</div>
			<?php else: ?>
				<div class="folder">
					<a class="flex-center" href="/admin/media/<?=$value;?>">
						<i class="fas fa-folder fa-10x"></i>
						<?=$value;?>
					</a>
				</div>
			<?php endif;
		endforeach; ?>
	</div>
<?php endif; ?>

<?php require ROOT . '/app/Views/admin/media/modal.php'; ?>