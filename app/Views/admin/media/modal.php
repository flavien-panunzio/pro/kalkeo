<div class="modal fade" id="Modal-files" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img class="w-100">
				<p class="d-none"></p>
			</div>
			<div class="modal-body">
				<form method="post" enctype="multipart/form-data" class="d-flex justify-content-between" id="upload-image-modal">
					<div class="custom-file">
						<label class="custom-file-label" for="customFile"><i class="fas fa-exchange-alt"></i> Changer d'image</label>
						<input type="file" class="custom-file-input" id="customFile" name="fileToUpload" accept="image/*" required>
					</div>
					<input type="submit" class="btn btn-success" value="Sauvegarder">
				</form>
			</div>
			<div class="modal-footer d-flex justify-content-between">
				<a class="btn btn-primary" id="dowload"><i class="fas fa-download"></i></a>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
			</div>
		</div>
	</div>
</div>