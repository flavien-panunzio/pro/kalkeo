<form method="post" enctype="multipart/form-data" class="d-flex justify-content-between" action="/admin/pdf">
	<div class="custom-file">
		<label class="custom-file-label" for="customFile"><i class="fas fa-exchange-alt"></i> Mettre à jour le PDF</label>
		<input type="file" class="custom-file-input" id="customFile" name="fileToUpload" accept=".pdf" required>
	</div>
	<input type="submit" class="btn btn-success" value="Sauvegarder">
</form>
<object class="w-100" data="/download/diplome-manon_roche.pdf" type="application/pdf">
	<iframe class="w-100" src="https://docs.google.com/viewer?url=<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]"?>/download/diplome-manon_roche.pdf&embedded=true"></iframe>
</object>