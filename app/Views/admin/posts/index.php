<h1 class="text-center">Accueil administration Kalkeo</h1>
<h2 class="text-center">MEMENTO</h2>
<h3>LIEUX</h3>
<ul>
	<li>Pour la description d’un lieu, le premier paragraphe est mis en avant automatiquement (pas besoin de le faire manuellement)</li>
	<li>La description visible sur la liste de tous les lieux équivaut au premier paragraphe de la description du lieu. Si cela excède les 150 caractères, la phrase sera coupée</li>
	<li>Si on change le slug, cela fait perdre tout le référencement acquis pour le lieu en question et il faut changer le nom de toutes les images associés au lieu</li>
	<li>Interdiction formelle d’écrire ||| quelque part dans la partie administration sous peine de tout casser...</li>
</ul>
<h3>BLOG</h3>
<ul>
	<li>Ne pas mettre de “Titre 1” lorsque tu rédige un blog, il est créer automatiquement avec le champs “Titre de l'article”. C’est vraiment mauvais d’avoir plusieurs “Titres 1”.</li>
	<li>Pour pouvoir mettre un “Titre 3” il faut qu’un “Titre 2” existe, pour avoir un titre plus grand, le gérer avec la taille du texte mais pas avec les “Titres”</li>
	<li>La touche “</>” permet d’afficher le code générer par l’outil de texte, cela peut-être pratique lorsque l’on souhaite modifier quelque chose de particulier (faire attention tout de même)</li>
</ul>
<h3>PHOTOS</h3>
<ul>
	<li>Ne pas avoir peur de <strong><u>COMPRESSER LES PHOTOS !!</u></strong> Avant de les mettre sur le site, une photo ne devra pas faire plus de 1Mo pour ça il y a plusieurs choses à faire :
		<ul>
			<li>Redimensionner la photo (1500px de large c’est déjà très bien)</li>
			<li>Aller sur compressor.io et compresser toutes les images (pas de perte de qualité)</li>
		</ul>
	</li>
	<li>Il est préférable d’avoir des photos au même format (paysage) et obligatoire d’avoir des photos en .jpg pour les lieux</li>
</ul>
<h3>OUTILS</h3>
<ul>
	<li><a href="https://analytics.google.com/analytics/web/">Google Analytics (statistiques du site)</a></li>
	<li><a href="https://business.google.com">Google MyBusiness</a></li>
	<li><a href="https://search.google.com/search-console/performance/search-analytics">Search Console (Suivi du référencement)</a></li>
</ul>