<?php 
	namespace App\Table;
	use Core\Table\Table;
	class CanyonTable extends Table {
		protected $table = "canyon";

		public function tout(){
			return $this->query("
				SELECT * FROM canyon
				UNION
				SELECT * FROM speleo
				ORDER BY id DESC");
		}

		public function nomColumn(){
			$temp = $this->query("
				SELECT COLUMN_NAME
				FROM INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_NAME = 'canyon'"
			);
			foreach ($temp as $key => $value) {
				$return[$key]=$value->COLUMN_NAME;
			}
			return $return;
		}

		public function toggle($value, $id){
			return $this->query("
				UPDATE $table
				SET visible = $value
				WHERE id = $id");
		}
	}