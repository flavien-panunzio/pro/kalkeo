<?php
	namespace App\Entity;
	use Core\Entity\Entity;

	class BlogEntity extends Entity{
		public function getExtrait(){
			$html = substr($this->contenu, 0, 500) . '...';
			return $html;
		}
	}