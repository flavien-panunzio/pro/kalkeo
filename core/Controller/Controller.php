<?php

	namespace Core\Controller;

	class Controller{

		protected $viewPath;
		protected $template;
		protected $titre="KalkeO, spéléologie et canyoning en région Grenobloise";
		protected $description="KalkeO, votre prestataire de sports de nature en Isère aux environs de Grenoble, idéale pour organiser des sorties de spéléologie ou de canyoning. Allez c’est parti !";
		protected $indexation='';

		protected function render($view, $variables = []){
			$titre=explode('-',$_SERVER['REQUEST_URI']);
			ob_start();
			extract($variables);
			require ($this->viewPath . str_replace('.','/',$view) . '.php');
			$content = ob_get_clean();
			require ($this->viewPath. 'templates/' . $this->template . '.php');
		}

		protected static function forbidden(){
			header("HTTP/1.0 403 Forbidden");
			header("Location: /login");
			die;
		}

		protected function loadModel($model_name){
			$this->$model_name = App::getInstance()->getTable($model_name);
		}
	}