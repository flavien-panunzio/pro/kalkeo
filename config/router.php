<?php
	return (object) array(
		'/'	 => 			[ 'class' => 'Posts',		'method' => 'index'],

		'/contact' =>		['class' => 'Posts',		'method' => 'contact'],

		'/speleologie' =>	['class' => 'Speleologie',	'method' => 'index'],

		'/canyoning' =>		['class' => 'Canyoning',	'method' => 'index'],

		'/equipe' =>		['class' => 'Posts',		'method' => 'equipe'],

		'/blog' => 			['class' => 'Blog',			'method' => 'index'],

		'/login' => 		['class' => 'Users',		'method' => 'login'],

		'/logout' => 		['class' => 'Users',		'method' => 'logout'],

		'/mentions-legales' =>	['class' => 'Posts',		'method' => 'mentions'],
		
		'/charte-de-qualite'=>	['class' => 'Posts',		'method' => 'charte'],
		
		'/cgv'				=>	['class' => 'Posts',		'method' => 'cgv'],

		'/erreur-418' => 	['class' => 'Errors',		'method' => 'error_418'],

		'/maintenance' =>	 ['class' => 'Errors',		'method' => 'maintenance'],

		'/admin' => [
				'index'	=>		['class' => 'Posts',	'method' => 'index'],

				'lieux'	=>		['class' => 'Lieux',	'method' => 'index'],

				'ajoutlieux'=>	['class' => 'Lieux',	'method' => 'add'],

				'modiflieux'=>	['class' => 'Lieux',	'method' => 'edit'],

				'supprlieux'=>	['class' => 'Lieux',	'method' => 'delete'],

				'visible'	=>	['class' => 'Lieux',		'method' => 'visible'],

				'blog'	=>		['class' => 'Blog',		'method' => 'index'],

				'ajoutblog'	=>		['class' => 'Blog',		'method' => 'add'],

				'modifblog'	=>		['class' => 'Blog',		'method' => 'edit'],

				'supprblog'	=>		['class' => 'Blog',		'method' => 'delete'],

				'equipe'	=>		['class' => 'Posts',		'method' => 'equipe'],

				'media'	=>		['class' => 'Media',		'method' => 'index'],

				'upload'	=>		['class' => 'Media',		'method' => 'upload'],

				'pdf'	=>		['class' => 'Media',		'method' => 'pdf']
		]
	);